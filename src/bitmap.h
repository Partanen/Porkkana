#ifndef BITMAP_H
#define BITMAP_H

/* Image and font stuctures
 * img_t and ttf_t are the pure bitmap data formats used by the program.
 * tex_t and sfont_t are structures with renderer id's associated with them -
 * they're declared here, but manipulated by functions in render.h and render.c.
 * The animation structures utilize the renderable structures - they are
 * generally pure data. */

#include "stb_rect_pack.h"
#include "stb_image.h"
#include "stb_truetype.h"
#include "types.h"

enum img_pxf_t
{
    IMG_RGB,
    IMG_RGBA,
    IMG_ALPHA
};

typedef struct ttf_t                ttf_t;
typedef struct img_t                img_t;
typedef struct tex_t                tex_t;              /* Renderable type */
typedef struct iso_anim_frame_t     iso_anim_frame_t;
typedef struct iso_anim_t           iso_anim_t;
typedef struct anim_frame_t         anim_frame_t;
typedef struct anim_t               anim_t;
typedef struct sfont_glyph_t        sfont_glyph_t;
typedef struct sfont_t              sfont_t;            /* Spritefont */

struct ttf_t
{
    stbtt_fontinfo      info;
    uchar               *bitmap;
    uint8               *file;
    int                 bitmap_w, bitmap_h;
    stbtt_packedchar    *glyphs; /* stbtt_packedchar */
    int                 num_glyphs; /* If info.numGlyphs < 256, this is 256 */
    int                 pixel_height;
    float               scale;
    int                 ascent; /* Ascent, descent and linegap are unscaled */
    int                 descent;/* (for the moment at least). To use, scale */
    int                 linegap;/* by font->scale first.                    */
};

struct img_t
{
    uint8           *pixels;
    int             w, h;
    enum img_pxf_t  pxf;
    int             bytes_per_px;
};

struct tex_t
{
    tex_id_t    id;
    float       w, h;
};

struct sfont_glyph_t
{
    float advance;
    float x_off, y_off; /* Offsets for calculating placement on a line */
    float clip[4];
    float left_side_bearing;
};

struct sfont_t
{
    tex_t           tex;
    sfont_glyph_t   *glyphs;
    int             num_glyphs;
    float           v_advance;
};

struct iso_anim_frame_t
{
    struct
    {
        tex_t           *tex;
        float           clip[4];
        float           ox, oy;
        int             flip;
    } tex_areas[8];
    float dur;
};

struct iso_anim_t
{
    iso_anim_frame_t    *frames;
    int                 num_frames;
    float               base_dur;
};

struct anim_frame_t
{
    tex_t   *tex;
    float   clip[4];
    float   ox, oy;
    int     flip;
    float   dur;
};

struct anim_t
{
    anim_frame_t    *frames;
    int             num_frames;
    float           total_dur;
};

int
img_load(img_t *img, const char *path);

void
img_free(img_t *img);

int
ttf_load(ttf_t *font, const char *path, int px_height, int h_oversample,
    int v_oversample);

void
ttf_free(ttf_t *font);

#endif /* BITMAP_H */
