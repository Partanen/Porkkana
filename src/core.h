#ifndef CORE_H
#define CORE_H

typedef struct gui_button_style_t   gui_button_style_t;
typedef struct gui_win_style_t      gui_win_style_t;
typedef struct sfont_t              sfont_t;

typedef struct screen_t screen_t;
typedef struct config_t config_t;

struct screen_t {
    const char  *name;
    void        (*update)(double dt);
    void        (*open)();
    void        (*close)();
};

extern struct config_t {
    int maximize_window;
    int window_size[2];
} config;

#define RESOLUTION_W 640
#define RESOLUTION_H 360

extern int main_window_w;
extern int main_window_h;
extern gui_button_style_t   *button_style1;
extern gui_win_style_t      *window_style1;
extern sfont_t              *default_font;
extern sfont_t              *small_font;

int core_init();
int core_run();
void core_shutdown();
float core_compute_target_viewport_scale();
float core_compute_target_viewport(int *ret);
void core_set_screen(screen_t *screen);
int core_is_key_down(int key);
int core_key_down_now(int key);
int core_key_up_now(int key);
void panic(int err, const char *comment);
unsigned int core_get_mouse_state(int *ret_x, int *ret_y);
int core_button_down_now(unsigned int button);
int core_button_up_now(unsigned int button);
unsigned int core_get_fps();

#endif /* CORE_H */
