#ifndef GUI_H
#define GUI_H

#include "types.h"

/* Forward declaration(s) */
typedef struct sfont_t  sfont_t;
typedef struct tex_t    tex_t;

enum gui_mouse_button_t
{
    GUI_MB_LEFT     = (1 << 0),
    GUI_MB_RIGHT    = (1 << 1),
    GUI_MB_MIDDLE   = (1 << 2),
    GUI_MB_NONE     = (1 << 31)
};

enum gui_win_flag_t
{
    GUI_WIN_CLICKTHROUGH = (1 << 0)
};

enum gui_origin_t
{
    GUI_TOP_LEFT,
    GUI_TOP_RIGHT,
    GUI_TOP_CENTER,
    GUI_BOTTOM_LEFT,
    GUI_BOTTOM_RIGHT,
    GUI_BOTTOM_CENTER,
    GUI_CENTER_LEFT,
    GUI_CENTER_RIGHT,
    GUI_CENTER_CENTER,

    NUM_GUI_ORIGINS
};

enum gui_flip_t
{
    GUI_FLIP_NONE,
    GUI_FLIP_H,
    GUI_FLIP_V,
    GUI_FLIP_BOTH
};

#define GUI_NUM_FLOATS_PER_VERT     5
#define GUI_NUM_FLOATS_PER_QUAD     (4 * GUI_NUM_FLOATS_PER_VERT)
#define GUI_VERT_SIZE               (GUI_NUM_FLOATS_PER_QUAD * sizeof(float))
#define GUI_MAX_MOUSE_BUTTONS       31

typedef struct gui_input_state_t            gui_input_state_t;
typedef struct gui_draw_cmd_t               gui_draw_cmd_t;
typedef struct gui_draw_list_t              gui_draw_list_t;
typedef struct gui_win_state_style_t        gui_win_state_style_t;
typedef struct gui_win_style_t              gui_win_style_t;
typedef struct gui_button_state_style_t     gui_button_state_style_t;
typedef struct gui_button_style_t           gui_button_style_t;
typedef struct gui_tex_button_state_style_t gui_tex_button_state_style_t;
typedef struct gui_tex_button_style_t       gui_tex_button_style_t;
typedef struct gui_anim_t                   gui_anim_t;
typedef struct gui_progressbar_style_t      gui_progressbar_style_t;

struct gui_input_state_t
{
    int     mx, my;
    uint32  buttons;
    int     coord_space[4]; /* x, y, w, h */
};

struct gui_draw_cmd_t
{
    uint    tex;
    int     num_verts;
};

struct gui_draw_list_t
{
    gui_draw_cmd_t  *cmds;
    int             num_cmds;
    float           *verts;
    int             num_verts;
    int             clip[4];
    int             inner_clip[4];
};

struct gui_win_state_style_t
{
    uint8               bg_col[4];                  /* Background */
    uint8               br_col[4];                  /* Border */
    int                 bw_t, bw_b, bw_l, bw_r;     /* Border widths */
    enum gui_origin_t   title_origin;
    int                 title_offset[2];
    float               title_scale[2];
    uint8               title_col[4];
    sfont_t             *title_font;
};

struct gui_win_style_t
{
    gui_win_state_style_t active;
    gui_win_state_style_t hovered;
    gui_win_state_style_t inactive;
};

struct gui_button_state_style_t
{
    uint8               bg_col[4];                  /* Background */
    uint8               br_col[4];                  /* Border */
    int                 bw_t, bw_b, bw_l, bw_r;     /* Border widths */
    enum gui_origin_t   title_origin;
    int                 title_offset[2];
    float               title_scale[2];
    sfont_t             *font;
};

struct gui_button_style_t
{
    gui_button_state_style_t normal;
    gui_button_state_style_t hovered;
    gui_button_state_style_t pressed;
};

struct gui_tex_button_state_style_t
{
    tex_t               *tex;
    float               clip[4];
    int                 tex_offset[2];
    enum gui_origin_t   title_origin;
    int                 title_offset[2];
    float               title_scale[2];
    sfont_t             *font;
};

struct gui_tex_button_style_t
{
    gui_tex_button_state_style_t normal;
    gui_tex_button_state_style_t hovered;
    gui_tex_button_state_style_t pressed;
};

struct gui_progressbar_style_t
{
    uint8                        bg_col[4];
    uint8                        fill_col[4];
    tex_t                        *tex;
    float                        clip[4];
    enum gui_origin_t            title_origin;
    int                          title_offset[2];
    float                        title_scale[2];
    sfont_t                      *font;
};

int
gui_init(int max_wins,
    void (*update_inputs)(gui_input_state_t *input_state),
    void (*render)(gui_draw_list_t *lists, int num_lists));

void
gui_destroy();

void
gui_clear();
/* Remove any existing windows. This may be handy when changing scenes. */

int
gui_begin();

int
gui_end();

bool32
gui_was_clicked();
/* Non-zero if the mouse was clicked down this frame and a non-clickthrough gui
 * element was under the cursor this frame, making it's window active. */

bool32
gui_was_released();
/* Non-zero if the gui was previously pressed, but no longer during this frame
 * */

#define gui_was_pressed() gui_is_any_element_pressed()

uint32
gui_get_active_win_id();

uint32
gui_set_active_win(const char *title);
/* Return value is the id of the window if successful, or 0 otherwise */

uint32
gui_is_any_element_pressed();
/* If any element is currently pressed, returns a mask of which buttons it was
 * clicked on by originally. */

uint32
gui_triggered_button();
/* Returns the id of the button triggered this frame (released), or 0 if none */

bool32
gui_is_a_button_pressed();

int
gui_cur_win_w();

int
gui_cur_win_h();

uint32
gui_begin_win(const char *title, int x, int y, int w, int h, int flags);

uint32
gui_begin_empty_win(const char *title, int x, int y, int w, int h, int flags);

int
gui_end_win();

int
gui_begin_guide(int x, int y, int w, int h);
/* Begin a rectangle to act as a new point of origin for all gui actions.
 * Relational to the currently active window. If the window ends, the guide
 * stack is wound back to zero.
 * Use this instead of gui_begin_empty_win() when you don't need a separate
 * draw list for the contenst of the rectangle. */

void
gui_end_guide();

int
gui_progressbar(const char *title, int x, int y, int w, int h);

int
gui_progressbar_tex(const char *title, int x, int y, int w, int h, int max_val);

int
gui_progressbar_tex_clipped(const char *title, int x, int y, int w, int h,
    int gable_w);

uint32
gui_repeat_button(const char *title, int x, int y, int w, int h,
    uint32 mbtn_mask);

uint32
gui_repeat_tex_button(const char *title, int x, int y, int w, int h,
    uint32 mbtn_mask);

uint32
gui_repeat_invisible_button(const char *title, int x, int y, int w, int h,
    uint32 mbtn_mask);

uint32
gui_button(const char *title, int x, int y, int w, int h, uint32 mbtn_mask);
/* Returns non-zero if the mouse button was pressed down and later released on
 * top of the button rectangle.
 * The parameter "mbtn_mask" tells the button which mouse buttons it is to
 * react to. Handly flags are GUI_MB_LEFT, GUI_MB_RIGHT and GUI_MB_MIDDLE.
 * If "mbtn_mask" is 0, a reaction to the left mouse button is assumed. To make
 * the button non-reactionary, the value GUI_MB_NONE can be used.
 * The return value is mbtn_mask ANDed with the released mouse flags - hence it
 * can be tested with the proper bitflags if, for example, the button was
 * clicked with the left or the right mouse button. */

uint32
gui_tex_button(const char *title, int x, int y, int w, int h, uint32 mbtn_mask);

uint32
gui_invisible_button(const char *title, int x, int y, int w, int h,
    uint32 mbtn_mask);

uint32
gui_region(const char *title, int x, int y, int w, int h);

int
gui_text(const char *text, int wrap, int x, int y);

int
gui_textf(const char *fmt, int wrap, int x, int y, ...);
/* Formatted text */

int
gui_text_s(const char *text, int wrap, int x, int y, float s);
/* Scaled text */

int
gui_get_active_text_input();

bool32
gui_text_input(int id, const char *text, int wrap, int x, int y, int w, int h);

int
gui_texture(tex_t *tex, float *clip, int x, int y);
/* Clip is allowed to be 0 */

int
gui_texture_s(tex_t *tex, float *clip, int x, int y, float sx, float sy);
/* Render a scaled texture */

int
gui_texture_c(tex_t *tex, float *clip, int x, int y, uint8 col[4]);

int
gui_texture_f(tex_t *tex, float *clip, int x, int y, enum gui_flip_t flip);

int
gui_texture_r(tex_t *tex, float *clip, int x, int y, float rot);

int
gui_texture_sc(tex_t *tex, float *clip, int x, int y,
    float sx, float sy, uint8 col[4]);

int
gui_texture_sf(tex_t *tex, float *clip, int x, int y, float sx, float sy,
    enum gui_flip_t flip);

int
gui_texture_sr(tex_t *tex, float *clip, int x, int y,
    float sx, float sy, float rot);

int
gui_texture_cf(tex_t *tex, float *clip, int x, int y, uint8 col[4],
    enum gui_flip_t flip);

int
gui_texture_cr(tex_t *tex, float *clip, int x, int y, uint8 col[4],
    float rot);

int
gui_texture_scf(tex_t *tex, float *clip, int x, int y,
    float sx, float sy, uint8 col[4], enum gui_flip_t flip);

int
gui_texture_scfr(tex_t *tex, float *clip, int x, int y,
    float sx, float sy, uint8 col[4], enum gui_flip_t flip, float rot);

int
gui_quad(int x, int y, int w, int h, uint8 *col);

void
gui_clicked_coord_inside_win(int *x, int *y);

void
gui_origin(enum gui_origin_t origin);

void
gui_font(sfont_t *font);

void
gui_text_color(uint8 r, uint8 g, uint8 b, uint8 a);

void
gui_win_style(gui_win_style_t *style);
/* If style is NULL, the default style will be used */

void
gui_button_style(gui_button_style_t *style);
/* If style is NULL, the default style will be used */

void
gui_progressbar_style(gui_progressbar_style_t *style);

void
gui_tex_button_style(gui_tex_button_style_t *style);

int gui_get_last_text_origin();
void gui_get_last_text_rect(int rect[4]);
/* Relative to origin. rect[2] and [3] are width and height */
int gui_get_last_text_x();
int gui_get_last_text_y();
int gui_get_last_text_w();
int gui_get_last_text_h();

int gui_get_last_button_origin();
void gui_get_last_button_rect(int rect[4]);
int gui_get_last_button_x();
int gui_get_last_button_y();
int gui_get_last_button_w();
int gui_get_last_button_h();

int gui_get_last_texture_origin();
void gui_get_last_texture_rect(int rect[4]);
int gui_get_last_texture_x();
int gui_get_last_texture_y();
int gui_get_last_texture_w();
int gui_get_last_texture_h();

int
gui_get_num_wins();

gui_win_style_t
gui_create_win_style();

gui_button_style_t
gui_create_button_style();

gui_tex_button_style_t
gui_create_tex_button_style();

gui_progressbar_style_t
gui_create_progressbar_style();

static inline int
set_gui_tex_button_state_style_clip(gui_tex_button_state_style_t *s, float *c);

static inline int
set_gui_tex_button_state_style_clip(gui_tex_button_state_style_t *s, float *c)
{
    if (s && c) {for (int i = 0; i < 4; ++i) s->clip[i] = c[i];  return 0;}
    return 1;
}

#endif /* GUI_H */
