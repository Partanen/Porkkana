#include "core.h"
#include "mainmenu.h"
#ifndef __ANDROID__
#include <SDL2/SDL.h>
#else
#include <SDL.h>
#endif
#include "util.h"


int main(int argc, char **argv)
{
    if (core_init())
        return 1;
    core_set_screen(mainmenu);
    if (core_run())
        return 2;
    return 0;
}
