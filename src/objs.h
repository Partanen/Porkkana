#ifndef OBSJ_H
#define OBSJ_H

typedef struct anim_t       anim_t;
typedef struct tex_t        tex_t;

typedef struct animator_t   animator_t;
typedef struct sprite_t     sprite_t;

enum sprite_type {
    SPRITE_PARALLAX,
    SPRITE_STATIC
};

struct animator_t {
    float               timer;
    float               speed;
    anim_t              *anim;
    int                 num_repeats;
    struct {
        unsigned int loop:1;
    } flags;
};

struct sprite_t {
    float               x, y;
    tex_t               *tex;
    float               clip[4];
    enum sprite_type    type;
};

void animator_play(animator_t *ar, anim_t *anim);
void animator_play_times(animator_t *ar, anim_t *anim, int num);
int animator_is_completed(animator_t *ar);
void update_animator_range(animator_t *ars, unsigned int num, float dt);
void init_animator_range(animator_t *ars, unsigned int num);
tex_t *animator_get_frame(animator_t *ar, float **ret_clip);

#endif /* OBSJ_H */
