#include "objs.h"
#include "bitmap.h"
#include <assert.h>

void animator_play(animator_t *ar, anim_t *anim)
{
    ar->timer       = 0.f;
    ar->anim        = anim;
    ar->num_repeats = 1;
}

void animator_play_times(animator_t *ar, anim_t *anim, int num)
{
    ar->timer       = 0.f;
    ar->anim        = anim;
    ar->num_repeats = num < 0 ? 0 : num;
}

int animator_is_completed(animator_t *ar)
{
    return !ar->anim || (ar->timer >= ar->anim->total_dur &&
        ar->num_repeats <= 0);
}

void update_animator_range(animator_t *ars, unsigned int num, float dt)
{
    animator_t *ar;
    for (unsigned int i = 0; i < num; ++i) {
        ar = &ars[i];
        float timer = ar->timer + ar->speed * dt;
        if (ar->anim && timer >= ar->anim->total_dur) {
            if (ar->flags.loop)
                timer -= ar->anim->total_dur;
            else {
                if (--ar->num_repeats > 0)
                    ar->timer = 0.f;
                else
                    ar->timer = ar->anim->total_dur;
                continue;
            }
        }
        ar->timer = timer;
    }
}

void init_animator_range(animator_t *ars, unsigned int num)
{
    animator_t *ar;
    for (unsigned int i = 0; i < num; ++i) {
        ar = &ars[i];
        ar->speed       = 1.f;
        ar->flags.loop  = 1;
        ar->num_repeats = 0;
    }
}

tex_t *animator_get_frame(animator_t *ar, float **ret_clip)
{
    if (!ar->anim)
        return 0;
    float   prcnt = (ar->timer / ar->anim->total_dur);
    int     index = prcnt  * (float)ar->anim->num_frames;
    if (index == ar->anim->num_frames)
        index = ar->anim->num_frames - 1;
    assert(index < ar->anim->num_frames);
    *ret_clip = ar->anim->frames[index].clip;
    return ar->anim->frames[index].tex;
}
