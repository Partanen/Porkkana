#ifndef __ANDROID__
#include <SDL2/SDL.h>
#else
#include <SDL.h>
#endif
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include "game.h"
#include "core.h"
#include "gui.h"
#include "render.h"
#include "assets.h"
#include "util.h"
#include "bitmap.h"
#include "objs.h"
#include "mainmenu.h"

#define BUNNY_INDEX(b_) ((int)((b_) - _state.bunnies))
#define TIMESTEP                    (1.f / 60.f)
#define MAX_CARROTS                 32
#define CARROT_GROWTH_STEP          2.5f
#define PLANT_DURATION              1.0f
#define COLLECT_DURATION            1.0f
#define CHEER_FREQUENCY             3.f
#define NUM_GROWTH_PHASES           4
#define CARROT_WIDTH                8
#define MAX_TEXT_EFFECTS            4
#define TEXT_EFFECT_DURATION        2.f
#define TEXT_EFFECT_START_Y         ((float)_state.ground_height + 20.f)
#define TEXT_EFFECT_TRAVEL_DISTANCE ((float)RESOLUTION_H * 0.5f)
#define MAX_SPRITES                 32
#define MAX_BUNNIES                 32

typedef struct carrot_t         carrot_t;
typedef struct text_effect_t    text_effect_t;
typedef struct bunny_t          bunny_t;

enum game_def_type {
    GAME_DEF_FOREGORUND_SPRITE,
    GAME_DEF_BACKGROUND_SPRITE,
    GAME_DEF_WELL,
    GAME_DEF_PLAYER
};

struct carrot_t {
    int     growth;
    int     x;
    double  timer;
};

struct text_effect_t {
    const char  *text;
    uint8       color[4];
    float       x;
    float       timer;
};

struct bunny_t {
    float x;
};

screen_t _game = {
    "game",
    game_update,
    game_open,
    0
};

enum player_action {
    PLAYER_ACTION_NONE,
    PLAYER_ACTION_WATERING,
    PLAYER_ACTION_PLANTING,
    PLAYER_ACTION_COLLECTING
};

screen_t                *game = &_game;
static double           _delta_accum;
static anim_t           *_player_walk_anim;
static anim_t           *_player_walk_bucket_anim;
static anim_t           *_player_idle_anim;
static anim_t           *_player_idle_bucket_anim;
static anim_t           *_player_watering_anim;
static anim_t           *_player_cheer_anim;
static anim_t           *_player_collect_anim;
static tex_t            *_player_tex;
static sprite_data_t    _carrot_sd[NUM_GROWTH_PHASES];
static int              _debug;
static int              _hitboxes_always;
static sprite_data_t    _ui_bar_sd;

static struct {
    int paused;
    int map_width;
    int ground_height;
    struct {
        double          x;
        double          target_x;
        int             y;
        int             w;
        animator_t      animator;
        enum sb_flip_t  flip;
        int             moved;
        int             action_state;
        double          action_timer;
        double          idle_timer;
        int             collected_carrot_index;
        unsigned int    num_carrots;
        int             can_in_hand;
        int             min_x, max_x;
    } player;
    struct {
        carrot_t        carrots[MAX_CARROTS];
        int             num;
    } carrots;
    struct {
        double          *current, max;
        const char      *text;
    } progress_bar;
    struct {
        text_effect_t   effects[MAX_TEXT_EFFECTS];
        int             num;
    } text_effects;
    struct {
        sprite_t    sprites[MAX_SPRITES];
        int         num;
    } sprites;
    struct {
        float x, y, min_x, max_x;
    } camera;
    struct {
        animator_t  animator;
        float       x, y;
    } well;
    struct {
        bunny_t     bunnies[MAX_BUNNIES];
        animator_t  animators[MAX_BUNNIES];
        int         num;
    } bunnies;
} _state;

static void _reset_state();
static void _move_player(double x);
static void _water();
static void _plant_carrot();
static void _collect_carrot();
static void _click_to_move();
static void _get_water_hit_range(int *x1, int *x2);
static void _get_collect_hit_range(int *x1, int *x2);
static void _get_carrot_hit_range(carrot_t *carrot, int *x1, int *x2);
static void _check_carrot_hit_ranges();
static void _start_progress_bar(double *current, double max, const char *text);
static void _stop_progress_bar();
static void _start_text_effect(const char *text, uint8 *color, float x);
static inline sprite_t *new_sprite();
static void _update_progress_bar();
static void _update_carrots(double dt);
static void _update_bunnies(double dt);
static void _update_player(double dt);
static void _update_text_effects(double dt);
static void _render_well(float cam_x, float cam_y);
static void _render_game();
static float _world_x_to_screen(float x);
static float _screen_x_to_world(float x);
static int _on_game_def(void *ctx, const char *def, const char *val);
static int _on_game_opt(void *ctx, const char *def, const char *val);
static void _set_camera_pos(float x, float y);
static bunny_t *_new_bunny(float x);
static void _stop_player_movement();

static void _start_text_effect(const char *text, uint8 *color, float x)
{
    if (_state.text_effects.num == MAX_TEXT_EFFECTS) {
        memmove(_state.text_effects.effects, _state.text_effects.effects + 1,
            (--_state.text_effects.num) * sizeof(text_effect_t));
    }
    text_effect_t *ef = &_state.text_effects.effects[_state.text_effects.num++];
    if (color)
        memcpy(ef->color, color, 4);
    else
        memset(ef->color, 0xFF, 4);
    ef->text = text;
    ef->x       = x;
    ef->timer   = 0.f;
}

static inline sprite_t *new_sprite()
{
    int num = _state.sprites.num;
    assert(num < MAX_SPRITES);
    _state.sprites.num = num + 1;
    memset(&_state.sprites.sprites[num], 0, sizeof(sprite_t));
    return &_state.sprites.sprites[num];
}

void game_open()
{
    _player_walk_anim           = as_get_anim("player_walk");
    _player_walk_bucket_anim    = as_get_anim("player_walk_bucket");
    _player_idle_anim           = as_get_anim("player_idle");
    _player_idle_bucket_anim    = as_get_anim("player_idle_bucket");
    _player_watering_anim       = as_get_anim("player_watering");
    _player_cheer_anim          = as_get_anim("player_cheer");
    _player_collect_anim        = as_get_anim("player_shovel_hit");
    _player_tex                 = as_get_tex("player");
    spritesheet_t *ss = as_get_spritesheet("objects");
    if (ss) {
        char buf[32];
        for (int i = 0; i < NUM_GROWTH_PHASES; ++i) {
            sprintf(buf, "carrot%d", i);
            _carrot_sd[i] = spritesheet_get_data(ss, buf);
        }
    }
    ss = as_get_spritesheet("ui");
    if (ss)
        _ui_bar_sd = spritesheet_get_data(ss, "bar");
    _reset_state();
    _debug              = 0;
    _hitboxes_always    = 0;
}

void game_update(double dt)
{
    float player_speed = 170.f;
    if (!_state.paused) {
        _delta_accum += dt;
        while (_delta_accum >= TIMESTEP) {
            if (_state.player.action_state == PLAYER_ACTION_NONE) {
                int left_down   = core_is_key_down(SDL_SCANCODE_LEFT);
                int right_down  = core_is_key_down(SDL_SCANCODE_RIGHT);
                int moved_with_keyboard = 0;
                if (!(left_down && right_down)) {
                    if (left_down) {
                        _move_player(TIMESTEP * -player_speed);
                        moved_with_keyboard = 1;
                    }
                    else if (right_down) {
                        _move_player(TIMESTEP * player_speed);
                        moved_with_keyboard = 1;
                    }
                }
                if (core_key_up_now(SDL_SCANCODE_SPACE))
                    _water();
                if (core_key_up_now(SDL_SCANCODE_C))
                    _plant_carrot();
                if (core_key_up_now(SDL_SCANCODE_X))
                    _collect_carrot();
                if (core_key_up_now(SDL_SCANCODE_F1))
                    _debug = _debug ? 0 : 1;
                if (core_key_up_now(SDL_SCANCODE_F3))
                    _hitboxes_always = _hitboxes_always ? 0 : 1;
                if (core_key_up_now(SDL_SCANCODE_ESCAPE))
                    _state.paused = 1;
                if (core_button_up_now(1))
                    _click_to_move();

                if (moved_with_keyboard)
                    _state.player.target_x = _state.player.x;
                else if (_state.player.target_x != _state.player.x) {
                    float d = _state.player.target_x - _state.player.x;
                    d = d < 0.f ? -1.f : 1.f;
                    _move_player(TIMESTEP * d * player_speed);
                    if ((d < 0.f && _state.player.x < _state.player.target_x) ||
                        (d > 0.f && _state.player.x > _state.player.target_x))
                        _state.player.x = _state.player.target_x;
                }
#ifdef _PORKKANA_DEBUG
                if (core_is_key_down(SDL_SCANCODE_F5)) {
                    _state.sprites.num = 0;
                    enum game_def_type def_type;
                    parse_def_file("game.def", _on_game_def, _on_game_opt,
                        &def_type);
                }
#endif
            }
            update_animator_range(&_state.player.animator, 1, TIMESTEP);
            update_animator_range(&_state.well.animator, 1, TIMESTEP);
            _update_carrots(TIMESTEP);
            _update_bunnies(TIMESTEP);
            _update_player(TIMESTEP);
            _update_text_effects(TIMESTEP);
            _delta_accum -= TIMESTEP;
        }
    }

    _set_camera_pos(_state.player.x, 0);

    /*-- GUI --*/
    gui_begin();
    gui_font(default_font);
    gui_button_style(button_style1);
    gui_win_style(window_style1);
    gui_origin(GUI_BOTTOM_LEFT);

    /* Player target position indicator */
    if (_state.player.target_x != _state.player.x) {
        int     wh      = 8;
        uint8   c[4]    = {0, 255, 0, 255};
        int     x       = _world_x_to_screen(_state.player.target_x) - wh / 2;
        int     y       = _state.ground_height + _state.player.y - wh / 2;
        gui_quad(x, y, wh, wh, c);
    }

    _update_progress_bar();

#if defined(_PORKKANA_DEBUG)
    if (_debug) {
        gui_origin(GUI_TOP_LEFT);

        int mx;
        core_get_mouse_state(&mx, 0);
        gui_textf("player: %f\ntarget: %f\ncursor: %f\nfps: %u", 0, 0, 0,
            _state.player.x, _state.player.target_x, _screen_x_to_world(mx),
            core_get_fps());

        DEBUG_PRINTFF("%d\n", gui_is_any_element_pressed());

        if (_state.player.action_state == PLAYER_ACTION_WATERING ||
            _hitboxes_always) {
                int wx1, wx2;
                _get_water_hit_range(&wx1, &wx2);
                uint8 color[4] = {255, 0, 0, 128};
                gui_origin(GUI_BOTTOM_LEFT);
                gui_quad(RESOLUTION_W / 2 + wx1, _state.ground_height,
                    wx2 - wx1, 32, color);
        }
        if (_state.player.action_state == PLAYER_ACTION_COLLECTING ||
            _hitboxes_always) {
            int cx1, cx2;
            _get_collect_hit_range(&cx1, &cx2);
            uint8 c[4] = {0, 255, 255, 255};
            gui_origin(GUI_BOTTOM_LEFT);
            gui_quad(RESOLUTION_W / 2 + cx1, _state.ground_height, cx2 - cx1,
                32, c);
        }
    }
#endif

#if defined(_PORKKANA_DEBUG)
    if (_debug) {
        gui_origin(GUI_BOTTOM_CENTER);
        uint8 c[4] = {0, 255, 0, 128};
        int num_carrots = _state.carrots.num;
        for (int i = 0; i < num_carrots; ++i)
            gui_quad((int)_state.carrots.carrots[i].x, _state.ground_height,
                CARROT_WIDTH, 16, c);
    }
#endif

    gui_origin(GUI_BOTTOM_CENTER);

    int bw = 64;
    int bh = 24;

    if (gui_button("Plant", -bw / 2 - 1 - bw, 4, bw, bh, 0) && !_state.paused)
        _plant_carrot();
    if (gui_button("Water", -bw / 2, 4, bw, bh, 0) && !_state.paused)
        _water();
    if (gui_button("Collect", bw / 2 + 1, 4, bw, bh, 0) && !_state.paused)
        _collect_carrot();
#ifdef __ANDROID__
    if (gui_repeat_button("<", -bw / 2 - bw * 2 - 2, 4, bw, bh, 0))
        _move_player(dt * -player_speed);
    if (gui_repeat_button(">", bw / 2 + bw + 2, 4, bw, bh, 0))
        _move_player(dt * player_speed);
#endif

    gui_origin(GUI_BOTTOM_RIGHT);
    if (gui_button("Menu", 4, 4, bw, bh, 0))
        _state.paused = 1;

    gui_origin(GUI_BOTTOM_LEFT);
    gui_text_color(255, 255, 255, 255);
    gui_textf("%u", 0, 4, 4, _state.player.num_carrots);

    gui_origin(GUI_BOTTOM_LEFT);

    int             num_text_effects = _state.text_effects.num;
    text_effect_t   *ef;
    float           text_y, alpha_mul;
    uint8           text_color[4], alpha;
    for (int i = 0; i < num_text_effects; ++i) {
        ef          = &_state.text_effects.effects[i];
        alpha_mul   = 1.f - ef->timer / TEXT_EFFECT_DURATION;
        alpha       = (uint8)((float)ef->color[3] / 255.f * alpha_mul * 255.f);
        text_y      = _state.ground_height + (ef->timer / TEXT_EFFECT_DURATION) *
            TEXT_EFFECT_TRAVEL_DISTANCE;
        text_color[0] = ef->color[0];
        text_color[1] = ef->color[1];
        text_color[2] = ef->color[2];
        text_color[3] = alpha;
        gui_text_color(text_color[0], text_color[1], text_color[2],
            text_color[3]);
        gui_font(small_font);
        gui_text(ef->text, 0, (int)_world_x_to_screen(ef->x), (int)text_y);
    }

    if (_state.player.collected_carrot_index >= 0) {
        gui_origin(GUI_BOTTOM_LEFT);
        int wx = _state.carrots.carrots[_state.player.collected_carrot_index].x;
        gui_text("x", 0, _world_x_to_screen(wx), _state.ground_height - 12);
    }

    if (_state.paused) {
        gui_origin(GUI_CENTER_CENTER);
        gui_begin_win("##pause_menu", 0, 0, 202, 108, 0);
        gui_text("Do you want to quit?", 0, 0, -12);
        int bw = 34;
        if (gui_button("Yes", -bw / 2 - 1, bw, bw, bw - 2, 0))
            core_set_screen(mainmenu);
        if (gui_button("No", bw / 2 + 1, bw, bw, bw - 2, 0))
            _state.paused = 0;
        gui_end_win();
    }

    r_viewport(0, 0, main_window_w, main_window_h);
    r_scissor(0, 0, main_window_w, main_window_h);
    r_color(0.f, 0.f, 0.f, 1.f);
    r_clear(R_CLEAR_COLOR_BIT);

    int vp[4];
    core_compute_target_viewport(vp);
    r_viewport(vp[0], vp[1], vp[2], vp[3]);
    r_scissor(vp[0], vp[1], vp[2], vp[3]);
    r_color(0.f, 0.f, 1.f, 1.f);
    r_clear(R_CLEAR_COLOR_BIT);

    _render_game();

    gui_end();
}

static void _reset_state()
{
    _state.player.w = 24;
    init_animator_range(&_state.player.animator, 1);
    animator_play(&_state.player.animator, _player_idle_anim);
    _state.player.animator.flags.loop   = 1;
    _state.player.animator.speed        = .5f;
    _state.player.flip                  = SB_FLIP_NONE;
    _state.player.moved                 = 0;
    _state.player.action_state          = PLAYER_ACTION_NONE;
    _state.player.idle_timer            = 0.f;
    _state.player.num_carrots           = 0;

    _state.carrots.num                  = 0;

    _state.bunnies.num                  = 0;

    _state.paused                       = 0;
    _state.player.action_timer          = 0.f;
    _state.text_effects.num             = 0;
    _state.player.can_in_hand           = 0;
    _state.sprites.num                  = 0;
    _state.map_width                    = RESOLUTION_W;
    _state.camera.y                     = 0.f;
    _set_camera_pos(_state.player.x, 0);

    init_animator_range(&_state.well.animator, 1);
    animator_play(&_state.well.animator, as_get_anim("well_idle"));

    enum game_def_type def_type;
    parse_def_file("game.def", _on_game_def, _on_game_opt, &def_type);
    _state.player.x         = _state.map_width / 2;
    _state.player.target_x  = _state.map_width / 2;

    _new_bunny(256);
}

static void _move_player(double x)
{
    if (_state.paused)
        return;
    _state.player.x += x;

    int max_x = _state.player.max_x;
    int min_x = _state.player.min_x;
    int limit = 0;

    if (_state.player.x > max_x) {
        _state.player.x = max_x;
        limit = 1;
    } else if (_state.player.x < min_x) {
        _state.player.x = min_x;
        limit = 1;
    }

    if (x > 0)
        _state.player.flip = SB_FLIP_NONE;
    else if (x < 0)
        _state.player.flip = SB_FLIP_H;

    if (x && !limit)
        _state.player.moved = 1;
}

static void _water()
{
    if (_state.player.action_state != PLAYER_ACTION_NONE)
        return;
    _stop_player_movement();
    _state.player.action_state = PLAYER_ACTION_WATERING;
    animator_play_times(&_state.player.animator, _player_watering_anim, 2);
    _state.player.animator.flags.loop   = 0;
    _state.player.can_in_hand           = 1;
}

static void _plant_carrot()
{
    if (_state.player.action_state != PLAYER_ACTION_NONE)
        return;
    _stop_player_movement();
    if (_state.carrots.num == MAX_CARROTS)
        return;
    _state.player.action_state = PLAYER_ACTION_PLANTING;
    _state.player.action_timer = 0.f;
    _start_progress_bar(&_state.player.action_timer, PLANT_DURATION,
        "Planting...");
    _state.player.can_in_hand = 0;
}

static void _collect_carrot()
{
    if (_state.player.action_state != PLAYER_ACTION_NONE)
        return;
    if (_state.player.collected_carrot_index < 0)
        return;
    _stop_player_movement();
    _state.player.action_state              = PLAYER_ACTION_COLLECTING;
    _state.player.action_timer              = 0.f;
    _start_progress_bar(&_state.player.action_timer, COLLECT_DURATION,
        "Collecting...");
    _state.player.can_in_hand = 0;
}

static void _click_to_move()
{
    int mx;
    core_get_mouse_state(&mx, 0);
    float target = _screen_x_to_world(mx);
    target = CLAMP(target, _state.player.min_x, _state.player.max_x);
    _state.player.target_x = target;
}

static void _get_water_hit_range(int *x1, int *x2)
{
    if (_state.player.flip == SB_FLIP_H) {
        *x1 = (int)_state.player.x - 32;
        *x2 = (int)_state.player.x;
    } else {
        *x1 = (int)_state.player.x + 0;
        *x2 = (int)_state.player.x + 32;
    }
}

static void _get_collect_hit_range(int *x1, int *x2)
{
    *x1 = (int)_state.player.x - 16;
    *x2 = (int)_state.player.x + 16;
}

static void _get_carrot_hit_range(carrot_t *carrot, int *x1, int *x2)
{
    *x1 = carrot->x - CARROT_WIDTH / 2;
    *x2 = carrot->x + CARROT_WIDTH / 2;
}

static void _check_carrot_hit_ranges()
{
    int px1, px2, cx1, cx2;
    _get_water_hit_range(&px1, &px2);

    int         num_carrots = _state.carrots.num;
    carrot_t    *carrot;

    for (int i = 0; i < num_carrots; ++i) {
        carrot = &_state.carrots.carrots[i];
        _get_carrot_hit_range(carrot, &cx1, &cx2);
        if (cx2 < px1)
            continue;
        if (cx1 > px2)
            continue;
        if (carrot->growth < NUM_GROWTH_PHASES - 1)
            carrot->growth++;
    }
}

static void _start_progress_bar(double *current, double max, const char *text)
{
    _state.progress_bar.current = current;
    _state.progress_bar.max     = max;
    _state.progress_bar.text    = text;
}

static void _stop_progress_bar()
    {_state.progress_bar.current = 0;}

static void _update_progress_bar()
{
    if (!_state.progress_bar.current)
        return;

    int w = 48;
    int h = 6;

    int x = (int)_state.player.x - _state.camera.x - w / 2 + RESOLUTION_W / 2;
    int y = _state.ground_height + 80;

    uint8 bg[4] = {0, 0, 0, 255};
    uint8 fg[4] = {255, 255, 255, 255};
    gui_quad(x, y, w, h, bg);

    float   max_len = (float)(w - 2);
    float   prcnt   = (float)(*_state.progress_bar.current /
        _state.progress_bar.max);
    int     len     = (int)(prcnt * max_len);
    gui_quad(x + 1, y + 1, len, h - 2, fg);
    gui_font(small_font);
    gui_text(_state.progress_bar.text, 0, x + 5, y + 8);
}

static void _update_carrots(double dt)
{
}

static void _update_bunnies(double dt)
{
    update_animator_range(_state.bunnies.animators, _state.bunnies.num, dt);
}

static void _update_player(double dt)
{
    switch (_state.player.action_state) {
    case PLAYER_ACTION_NONE: {
        if (_state.player.moved) {
            if (_state.player.animator.anim != _player_walk_anim) {
                animator_play(&_state.player.animator, _player_walk_anim);
                _state.player.animator.flags.loop   = 1;
                _state.player.idle_timer            = 0;
            }
        } else {
            if (!_state.player.animator.flags.loop &&
                !animator_is_completed(&_state.player.animator))
                break;
            if (_state.player.animator.anim != _player_idle_anim) {
                animator_play(&_state.player.animator, _player_idle_anim);
                _state.player.animator.flags.loop   = 1;
                _state.player.idle_timer            = 0.f;
            }
            _state.player.idle_timer += dt;
            if (_state.player.idle_timer < CHEER_FREQUENCY)
                break;
            animator_play_times(&_state.player.animator, _player_cheer_anim,
                rand() % 3);
            _state.player.animator.flags.loop   = 0;
            _state.player.idle_timer            = 0.f;
        }
    }
        break;
    case PLAYER_ACTION_WATERING: {
        if (animator_is_completed(&_state.player.animator)) {
            _state.player.action_state = PLAYER_ACTION_NONE;
            animator_play(&_state.player.animator, _player_idle_anim);
            _state.player.animator.flags.loop = 1;
            _check_carrot_hit_ranges();
        }
    }
        break;
    case PLAYER_ACTION_PLANTING: {
        if (_state.player.animator.anim != _player_idle_anim) {
            animator_play(&_state.player.animator, _player_idle_anim);
            _state.player.animator.flags.loop = 1;
        }
        _state.player.action_timer += dt;
        if (_state.player.action_timer < PLANT_DURATION)
            break;
        _state.player.action_state = PLAYER_ACTION_NONE;
        carrot_t *carrot = &_state.carrots.carrots[_state.carrots.num++];
        carrot->x       = (int)_state.player.x;
        carrot->growth  = 0;
        _stop_progress_bar();
    }
        break;
    case PLAYER_ACTION_COLLECTING: {
        if (_state.player.animator.anim != _player_collect_anim) {
            animator_play(&_state.player.animator, _player_collect_anim);
            _state.player.animator.flags.loop = 1;
        }
        _state.player.action_timer += dt;
        if (_state.player.action_timer < COLLECT_DURATION)
            break;
        int index = _state.player.collected_carrot_index;
        _state.player.action_state = PLAYER_ACTION_NONE;
        _stop_progress_bar();
        if (_state.carrots.carrots[index].growth < NUM_GROWTH_PHASES - 1) {
            _start_text_effect("Failed!", 0, (float)_state.player.x);
            break;
        }
        _state.carrots.carrots[index] = _state.carrots.carrots[
            --_state.carrots.num];
        _start_text_effect("+1", 0, (float)_state.player.x);
        _state.player.num_carrots++;
    }
        break;
    }
    _state.player.moved = 0;

    int num_carrots = _state.carrots.num;
    int index       = -1;
    int px1, px2, cx1, cx2;
    _get_collect_hit_range(&px1, &px2);
    for (int i = 0; i < num_carrots; ++i) {
        _get_carrot_hit_range(&_state.carrots.carrots[i], &cx1, &cx2);
        if (cx2 < px1)
            continue;
        if (cx1 > px2)
            continue;
        index = i;
        break;
    }
    _state.player.collected_carrot_index = index;
}

static void _update_text_effects(double dt)
{
    float           fdt                 = (float)dt;
    int             num_text_effects    = _state.text_effects.num;
    text_effect_t   *effects            = _state.text_effects.effects;
    for (int i = 0; i < num_text_effects; ++i) {
        effects[i].timer += fdt;
        if (effects[i].timer < TEXT_EFFECT_DURATION)
            continue;
        memmove(effects + i, effects + i + 1,
            ((--_state.text_effects.num) - i) * sizeof(text_effect_t));
            i++;
    }
}

static void _render_sprites(float cam_x, float cam_y)
{
    int         num_sprites = _state.sprites.num;
    sprite_t    *s;

    int     cw;
    int     diff;
    int     x;
    float   p;

    for (int i = 0; i < num_sprites; ++i) {
        s = &_state.sprites.sprites[i];
        assert(s->tex);
        if (!s->tex)
            continue;
        if (s->type == SPRITE_PARALLAX) {
            cw      = s->clip[2] - s->clip[0];
            diff    = cw - RESOLUTION_W;
            p       = (_state.camera.x - _state.camera.min_x) / _state.camera.max_x;
            x       = -p * (float)diff;
        } else
            x = _world_x_to_screen(s->x);
        sb_sprite(s->tex, s->clip, x, s->y - cam_y, 0);
    }
}

static void _render_well(float cam_x, float cam_y)
{
    float   *clip;
    tex_t   *tex = animator_get_frame(&_state.well.animator, &clip);
    sb_sprite(tex, clip, _world_x_to_screen(_state.well.x), _state.well.y, 0.f);
}

static void _render_bunnies(float cam_x, float cam_y)
{
    int     num = _state.bunnies.num;
    float   *clip;
    tex_t   *tex;

    for (int i = 0; i < num; ++i) {
        tex = animator_get_frame(&_state.bunnies.animators[i], &clip);
        if (!tex) {
            continue;
        }
        sb_sprite(tex, clip, _world_x_to_screen(_state.bunnies.bunnies[i].x),
            RESOLUTION_H - _state.ground_height - clip[3] - clip[1], 0);
    }
}

static void _render_game()
{
    float cam_x = _state.camera.x;
    float cam_y = _state.camera.y;

    float fvp[4] = {0, 0, RESOLUTION_W, RESOLUTION_H};
    sb_begin(0, fvp);

    _render_sprites(cam_x, cam_y);
    _render_well(cam_x, cam_y);
    _render_bunnies(cam_x, cam_y);

    float   *clip;
    tex_t   *tex = animator_get_frame(&_state.player.animator, &clip);
    int x = _state.player.x - (clip[2] - clip[0]) / 2 - cam_x + RESOLUTION_W / 2;
    int y = RESOLUTION_H - _state.ground_height + _state.player.y -
        (clip[3] - clip[1]) - cam_y;
    sb_sprite_f(tex, clip, x, y, 0.f, _state.player.flip);

    carrot_t    *carrots        = _state.carrots.carrots;
    int         num_carrots     = _state.carrots.num;
    float       *carrot_clip    = _carrot_sd[2].clip;
    int         chw             = (int)(carrot_clip[2] - carrot_clip[0]) / 2;
    int         ch              = (int)(carrot_clip[3] - carrot_clip[1]);

    uint8       full[4]         = {255, 255, 255, 255};
    uint8       alph[4]         = {255, 255, 255, 128};
    int         s               = _state.player.collected_carrot_index;

    for (int i = 0; i < num_carrots; ++i) {
        carrot_clip = _carrot_sd[carrots[i].growth].clip;
        chw         = (int)(carrot_clip[2] - carrot_clip[0]) / 2;
        ch          = (int)(carrot_clip[3] - carrot_clip[1]);
        x           = RESOLUTION_W / 2 + carrots[i].x - chw - cam_x;
        y           = RESOLUTION_H - _state.ground_height - ch - cam_y;
        sb_sprite_c(_carrot_sd[carrots[i].growth].tex, carrot_clip, x, y, 0.f,
            i != s ? full : alph);
    }

    sb_end(0);
}

static float _world_x_to_screen(float x)
    {return x - _state.camera.x + RESOLUTION_W / 2;}

static float _screen_x_to_world(float x)
{
    float s = core_compute_target_viewport_scale();
    return x / (s ? s : 1.f) + _state.camera.x - RESOLUTION_W / 2;
}

static int _on_game_def(void *ctx, const char *def, const char *val)
{
    enum game_def_type *def_type = (enum game_def_type*)ctx;
    if (streq(def, "background_sprite")) {
        sprite_t *s = new_sprite(def);
        s->type = SPRITE_PARALLAX;
        *def_type = GAME_DEF_BACKGROUND_SPRITE;
    } else if (streq(def, "foreground_sprite")) {
        sprite_t *s = new_sprite(def);
        s->type = SPRITE_STATIC;
        *def_type = GAME_DEF_FOREGORUND_SPRITE;
    } else if (streq(def, "map_width"))
        _state.map_width = atoi(val);
    else if (streq(def, "well"))
        *def_type = GAME_DEF_WELL;
    else if (streq(def, "camera_min_x_edge"))
        _state.camera.min_x = atoi(val) + RESOLUTION_W / 2;
    else if (streq(def, "camera_max_x_edge"))
        _state.camera.max_x = atoi(val) - RESOLUTION_W / 2;
    else if (streq(def, "player"))
        *def_type = GAME_DEF_PLAYER;
    else if (streq(def, "ground_height"))
        _state.ground_height = atoi(val);
    return 0;
}

static int _on_game_opt(void *ctx, const char *opt, const char *val)
{
    if (!_state.sprites.num)
        return 1;

    enum game_def_type def_type = *(enum game_def_type*)ctx;

    sprite_t *s = &_state.sprites.sprites[_state.sprites.num - 1];

    switch (def_type) {
    case GAME_DEF_FOREGORUND_SPRITE:
    case GAME_DEF_BACKGROUND_SPRITE:
        if (streq(opt, "tex")) {
            s->tex = as_get_tex(val);
        } else if (streq(opt, "x"))
            s->x = atof(val);
        else if (streq(opt, "y"))
            s->y = atof(val);
        else if (streq(opt, "clip_x1"))
            s->clip[0] = atof(val);
        else if (streq(opt, "clip_y1"))
            s->clip[1] = atof(val);
        else if (streq(opt, "clip_x2"))
            s->clip[2] = atof(val);
        else if (streq(opt, "clip_y2"))
            s->clip[3] = atof(val);
        else
            return 2;
        break;
    case GAME_DEF_WELL:
        if (streq(opt, "x"))
            _state.well.x = (int)atoi(val);
        else if (streq(opt, "y"))
            _state.well.y = (int)atoi(val);
        else
            return 3;
        break;
    case GAME_DEF_PLAYER:
        if (streq(opt, "x"))
            _state.player.x = atoi(val);
        else if (streq(opt, "y"))
            _state.player.y = atoi(val);
        else if (streq(opt, "min_x"))
            _state.player.min_x = atoi(val);
        else if (streq(opt, "max_x"))
            _state.player.max_x = atoi(val);
        break;
    }
    return 0;
}

static void _set_camera_pos(float x, float y)
{
    if (x < _state.camera.min_x)
        x = _state.camera.min_x;
    if (x > _state.camera.max_x)
        x = _state.camera.max_x;
    _state.camera.x = x;
    _state.camera.y = y;
}

static bunny_t *_new_bunny(float x)
{
    assert(_state.bunnies.num != MAX_BUNNIES);
    int     index   = _state.bunnies.num;
    bunny_t *bunny  = &_state.bunnies.bunnies[index];
    bunny->x        = x;
    init_animator_range(&_state.bunnies.animators[index], 0);
    animator_play(&_state.bunnies.animators[index], as_get_anim("bunny_idle"));
    _state.bunnies.animators[index].flags.loop  = 1;
    _state.bunnies.num                          = index + 1;
    return bunny;
}

static void _stop_player_movement()
    {_state.player.target_x = _state.player.x;}
