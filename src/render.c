#ifndef __ANDROID__
#include <GL/glew.h>
#include <SDL2/SDL.h>
#else
#include <GLES2/gl2.h>
#include <SDL.h>
#define GL_MAX_TEXTURE_UNITS    8
#define GL_CLAMP                GL_CLAMP_TO_EDGE
#define GL_CLAMP_TO_BORDER      GL_CLAMP_TO_EDGE
#endif
#include <assert.h>
#include <math.h>
#include <string.h>
#include "render.h"
#include "gui.h"
#include "bitmap.h"
#include "util.h"

#define GLS_ACTIVE_TEX_INDEX() (gl_state.active_texture - GL_TEXTURE0)

#if SB_USE_MAPPED_VBOS
    #define SB_BUFFER_USAGE GL_STREAM_DRAW
#else
    #define SB_BUFFER_USAGE GL_DYNAMIC_DRAW
#endif

typedef struct spritebatch_t spritebatch_t;

struct sb_cmd_t {
    int     num_repeats;
    uint32  tex;
};

struct spritebatch_t {
    int         began;
    uint32      vbo;
    uint32      ebo;
    int         vbo_offset;
    float       *mapped_vbo;
    sb_cmd_t    *cmds;
    int         max_cmds;
    int         buf_size;
    int         num_cmds;
    int         repeat_count;
    int         num_floats;
    int         max_floats; /* Saved for possible space checks */
    float       coord_space[4];
    uint32      shader;
    int         index_type;
};

int R_CLEAR_COLOR_BIT = GL_COLOR_BUFFER_BIT;
int R_CLEAR_DEPTH_BIT = GL_DEPTH_BUFFER_BIT;

static SDL_GLContext    _gl_context;
static uint32           _bound_tex2ds[GL_MAX_TEXTURE_UNITS];
static const uint8      _sb_col_white[4] = {255, 255, 255, 255};
static GLuint           _gui_shader;
static tex_t            _white_dummy_tex;
static GLuint           _gui_vbo;
static GLuint           _gui_vbo_size;
static GLuint           _gui_ebo;
static spritebatch_t    _sb;
static GLuint           _sb_opaq_shader;
static GLuint           _sb_blend_shader;

static struct gl_state_t {
    int     active_texture;
    uint32  bound_vbo;
    uint32  bound_ebo;
    uint32  bound_program;
    int     blend_sfactor;
    int     blend_dfactor;
    bool32  blend_enabled;
    bool32  depth_test_enabled;
    bool32  scissor_enabled;
    uint32  *bound_tex2ds;
    int32   viewport[4];
    int32   scissor[4];
} gl_state;

static inline GLenum
_img_pxf_to_gl_enum(enum img_pxf_t pxf);

static void
_init_gl_state();

static inline void
_bind_gui_vao();

static inline void
_bind_sb_vao();

static inline void
gls_use_program(GLuint program);

static inline void
gls_bind_vbo(GLuint vbo);

static inline void
gls_bind_ebo(GLuint ebo);

static inline void
gls_active_texture(GLenum tex);

static inline void
gls_enable_blend();

static inline void
gls_disable_blend();

static inline void
gls_blend_func(GLenum sfactor, GLenum dfactor);

static inline void
gls_enable_depth_test();

static inline void
gls_disable_depth_test();

static inline void
gls_enable_scissor();

static inline void
gls_bind_tex2d(GLuint tex);

static inline void
gls_bind_tex2d_if_not_bound(GLuint tex);

void print_gl_errors()
{
    GLenum error = glGetError();
    if (error != GL_NO_ERROR) {
        puts("GL errors found! Printing them out...");
        printf("%i\n", error);
        for (error = glGetError(); error != GL_NO_ERROR; error = glGetError())
            printf("%i\n", error);
        puts("Printed all errors!");
    }
}

GLuint shader_from_strs(const char *vtx_code, const char *frg_code,
    char *err_log, int err_log_len)
{
    if (!vtx_code || !frg_code)
        return 0;

    GLuint vtx_sh = glCreateShader(GL_VERTEX_SHADER);
    GLuint frg_sh = glCreateShader(GL_FRAGMENT_SHADER);

    glShaderSource(vtx_sh, 1, &vtx_code, 0);
    glShaderSource(frg_sh, 1, &frg_code, 0);
    glCompileShader(vtx_sh);
    glCompileShader(frg_sh);

    GLint success;

    glGetShaderiv(vtx_sh, GL_COMPILE_STATUS, &success);

    if (!success) {
        if (err_log)
            glGetShaderInfoLog(vtx_sh, err_log_len, 0, err_log);
        return 0;
    }

    glGetShaderiv(frg_sh, GL_COMPILE_STATUS, &success);

    if (!success) {
        if (err_log)
            glGetShaderInfoLog(frg_sh, err_log_len, 0, err_log);
        return 0;
    }

    GLuint prog = glCreateProgram();

    glAttachShader(prog, vtx_sh);
    glAttachShader(prog, frg_sh);

    glBindAttribLocation(prog, SHADER_ATTR_LOC_POS, "position");
    glBindAttribLocation(prog, SHADER_ATTR_LOC_UV,  "in_uv");
    glBindAttribLocation(prog, SHADER_ATTR_LOC_COL, "in_color");

    glLinkProgram(prog);
    glDeleteShader(vtx_sh);
    glDeleteShader(frg_sh);

    glGetProgramiv(prog, GL_LINK_STATUS, &success);

    if (!success) {
        if (err_log)
            glGetProgramInfoLog(prog, err_log_len, 0, err_log);
        return 0;
    }

    return prog;
}

GLuint shader_from_files(const char *vert_path, const char *frag_path,
    char *err_log, int err_log_len)
{
    dchar *vert = load_text_file_to_dstr(vert_path);
    dchar *frag = load_text_file_to_dstr(frag_path);
    GLuint ret = shader_from_strs(vert, frag, err_log, err_log_len);
    free(vert);
    free(frag);
    return ret;
}

int init_graphics_api(void *sdl_window)
{
#ifdef __ANDROID__
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
        SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    _gl_context = SDL_GL_CreateContext((SDL_Window*)sdl_window);
#else
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
        SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
    _gl_context = SDL_GL_CreateContext((SDL_Window*)sdl_window);
    GLenum glew_result  = glewInit();
    if (glew_result != GLEW_OK)
        return 3;
#endif
    return 0;
}

int tex_from_mem(tex_t *tex, uint8 *pixels, int w, int h, int pxf)
{
    return tex_from_mem_ext(tex, pixels, w, h, pxf, TEX_FILTER_LINEAR,
        TEX_FILTER_NEAREST, TEX_WRAP_REPEAT);
}

int tex_from_mem_ext(tex_t *tex, uint8 *pixels, int w, int h,
    int pxf,
    enum tex_filter_t min_filter,
    enum tex_filter_t mag_filter,
    enum tex_wrapping_t wrapping)
{
    if (!tex)
        return 1;
    GLenum min_filt  = min_filter == TEX_FILTER_LINEAR  ? GL_LINEAR  : GL_NEAREST;
    GLenum mag_filt  = mag_filter == TEX_FILTER_NEAREST ? GL_NEAREST : GL_LINEAR;
    GLenum wrap_type = 0;

    switch (wrapping) {
        default:
        case TEX_WRAP_REPEAT:          wrap_type = GL_REPEAT;          break;
        case TEX_WRAP_MIRROR_REPEAT:   wrap_type = GL_MIRRORED_REPEAT; break;
        case TEX_WRAP_CLAMP:           wrap_type = GL_CLAMP;           break;
        case TEX_WRAP_CLAMP_TO_EDGE:   wrap_type = GL_CLAMP_TO_EDGE;   break;
        case TEX_WRAP_CLAMP_TO_BORDER: wrap_type = GL_CLAMP_TO_BORDER; break;
    };

    glGenTextures(1, &tex->id);
    glActiveTexture(GL_TEXTURE0);
    gls_bind_tex2d(tex->id);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, min_filt);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mag_filt);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap_type);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap_type);

    GLenum px_format = _img_pxf_to_gl_enum(pxf);

    glTexImage2D(GL_TEXTURE_2D, 0, px_format, w, h, 0, px_format,
        GL_UNSIGNED_BYTE, pixels);
    gls_bind_tex2d(0);

    tex->w = (GLfloat)w;
    tex->h = (GLfloat)h;

    return 0;
}

int tex_from_img(tex_t *tex, img_t *img)
{
    if (!img)
        return 1;
    int res = tex_from_mem(tex, img->pixels, img->w, img->h, img->pxf);
    if (res != 0)
        return (res + 1);
    return 0;
}

int tex_from_img_ext(tex_t *tex, img_t *img, int min_filter, int mag_filter,
    int wrapping)
{
    if (!img)
        return 1;
    int res = tex_from_mem_ext(tex, img->pixels, img->w, img->h, img->pxf,
        min_filter, mag_filter, wrapping);
    return res ? res + 1 : 0;
}

void tex_free(tex_t *tex)
{
    if (!tex) return;
    glDeleteTextures(1, &tex->id);
    memset(tex, 0, sizeof(tex_t));
}

int sfont_from_ttf(sfont_t *sfont, ttf_t *ttf)
{
    return sfont_from_ttf_ext(sfont, ttf, TEX_FILTER_NEAREST, TEX_FILTER_NEAREST,
        TEX_WRAP_REPEAT);
}

int sfont_from_ttf_ext(sfont_t *sfont, ttf_t *ttf,
    enum tex_filter_t min_filter,
    enum tex_filter_t mag_filter,
    enum tex_wrapping_t wrapping)
{
    if (!sfont || !ttf)
        return 1;

    sfont_glyph_t *glyphs = malloc(ttf->num_glyphs * sizeof(sfont_glyph_t));

    if (!glyphs)
        return 2;

    if (tex_from_mem_ext(&sfont->tex, ttf->bitmap, ttf->bitmap_w, ttf->bitmap_h,
        IMG_RGBA, min_filter, mag_filter, wrapping) != 0) {
        free(glyphs);
        return 3;
    }

    sfont_glyph_t *g;
    int tmp_advance, tmp_left_side_bearing;

    for (int i = 0; i < ttf->num_glyphs; ++i) {
        stbtt_GetGlyphHMetrics(&ttf->info, i, &tmp_advance,
            &tmp_left_side_bearing);

        g                       = &glyphs[i];
        g->clip[0]              = ttf->glyphs[i].x0;
        g->clip[1]              = ttf->glyphs[i].y0;
        g->clip[2]              = ttf->glyphs[i].x1;
        g->clip[3]              = ttf->glyphs[i].y1;
        g->advance              = ROUNDF(ttf->glyphs[i].xadvance);
        g->left_side_bearing    = (float)tmp_left_side_bearing;
        g->x_off                = ROUNDF((float)ttf->glyphs[i].xoff2);
        g->y_off                = ROUNDF((float)ttf->glyphs[i].yoff2);
    }

    sfont->glyphs       = glyphs;
    sfont->num_glyphs   = ttf->num_glyphs;
    sfont->v_advance    = ROUNDF(ttf->scale * ((float)ttf->ascent - \
        (float)ttf->descent + (float)ttf->linegap));

    return 0;
}

void sfont_free(sfont_t *sfont)
{
    if (!sfont)
        return;
    free(sfont->glyphs);
    tex_free(&sfont->tex);
    memset(sfont, 0, sizeof(sfont_t));
}

int sb_init(int max_sprites, char *err_log, int err_log_len)
{
    if (err_log)
        err_log[0] = 0;

    if (max_sprites <= 0)
        return 1;

    _sb.cmds = malloc(max_sprites * sizeof(sb_cmd_t));
    if (!_sb.cmds)
        return 2;

    /* Create the default shader */
    const char *vtx_code =
        "#version 100\n"
        "precision lowp float;"
        "attribute vec3 position;"
        "attribute vec2 in_uv;"
        "attribute vec4 in_color;"
        "varying vec2 uv;"
        "varying vec4 color;"
        "uniform mat4 projection;"
        "void main(void)"
        "{"
            "gl_Position    = projection * vec4(position, 1.0);"
            "uv             = in_uv;"
            "color          = in_color;"
        "}";

    const char *frg_code =
        "#version 100\n"
        "precision lowp float;"
        "varying vec4 color;"
        "varying vec2 uv;"
        "uniform sampler2D tex;"
        "void main(void)\n"
        "{"
            "vec4 frag_color = texture2D(tex, uv) * color;"
            "if (frag_color.w == 0.0) discard;"
            "gl_FragColor = frag_color;"
        "}";

    _sb_opaq_shader = shader_from_strs(vtx_code, frg_code, err_log,
        err_log_len);

    if (glGetError()) {
        DEBUG_PRINTFF("%d\n", glGetError());
        return 3;
    }

    /* Create the defaut blended shader */
    frg_code =
        "#version 100\n"
        "precision lowp float;"
        "varying vec2 uv;"
        "varying vec4 color;"
        "uniform sampler2D tex;"
        "void main(void)\n"
        "{"
        "   gl_FragColor = texture2D(tex, uv) * color;"
        "}";

    _sb_blend_shader = shader_from_strs(vtx_code, frg_code, err_log,
        err_log_len);

    if (glGetError())
        return 4;

    /* Gen buffers */
    glGenBuffers(1, &_sb.vbo);
    gls_bind_vbo(_sb.vbo);
    glBufferData(GL_ARRAY_BUFFER, max_sprites * SIZE_OF_SPRITE, 0,
        SB_BUFFER_USAGE);

#if !SB_USE_MAPPED_VBOS
    void *vbo_mem = malloc(max_sprites * SIZE_OF_SPRITE);
    if (!vbo_mem)
        return 5;
    _sb.mapped_vbo = (GLfloat*)vbo_mem;
#endif

    GLint loc_position  = glGetAttribLocation(_sb_opaq_shader, "position");
    if (loc_position < 0) return 6;
    GLint loc_tex_coord = glGetAttribLocation(_sb_opaq_shader, "in_uv");
    if (loc_tex_coord < 0) return 7;

    glVertexAttribPointer(SHADER_ATTR_LOC_POS, 3, GL_FLOAT, GL_FALSE,
        6 * sizeof(GLfloat), 0);
    glVertexAttribPointer(SHADER_ATTR_LOC_UV, 2, GL_FLOAT, GL_FALSE,
        6 * sizeof(GLfloat), (const GLvoid *)(3 * sizeof(GLfloat)));
    glVertexAttribPointer(SHADER_ATTR_LOC_COL, 4, GL_UNSIGNED_BYTE, GL_TRUE,
        6 * sizeof(GLfloat), (const GLvoid *)(5 * sizeof(GLfloat)));
    glEnableVertexAttribArray(SHADER_ATTR_LOC_UV);
    glEnableVertexAttribArray(SHADER_ATTR_LOC_POS);
    glEnableVertexAttribArray(SHADER_ATTR_LOC_COL);

    /* Element array buffer */
    glGenBuffers(1, &_sb.ebo);
    gls_bind_ebo(_sb.ebo);

    GLsizei single_index_size;

    int max_verts = max_sprites * 6;

    if (max_verts <= 256) {
        single_index_size = sizeof(GLubyte);
        _sb.index_type = GL_UNSIGNED_BYTE;
    } else if (max_verts <= 0xFFFF)
    {
        single_index_size = sizeof(GLushort);
        _sb.index_type = GL_UNSIGNED_SHORT;
    } else {
        single_index_size = sizeof(GLuint);
        _sb.index_type = GL_UNSIGNED_INT;
    }

#if SB_USE_MAPPED_VBOS
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, max_verts * single_index_size, 0,
        SB_BUFFER_USAGE);
    void *ebo_data = glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);
#else
    void *ebo_data = calloc(max_verts, single_index_size);
    /* TODO: return on failure */
#endif

    switch (_sb.index_type) {
        case GL_UNSIGNED_BYTE: {
            GLubyte indices[6] = {0, 1, 2, 2, 3, 1};
            for (GLubyte i = 0; i < (GLubyte)max_sprites; ++i)
                for (GLubyte j = 0; j < 6; ++j)
                    ((GLubyte*)ebo_data)[i * 6 + j] = indices[j] + i * 4;
        }
            break;
        case GL_UNSIGNED_SHORT: {
            GLushort indices[6] = {0, 1, 2, 2, 3, 1};
            for (GLushort i = 0; i < (GLushort)max_sprites; ++i)
                for (GLushort j = 0; j < 6; ++j)
                    ((GLushort*)ebo_data)[i * 6 + j] = indices[j] + i * 4;
        }
            break;
        case GL_UNSIGNED_INT: {
            GLuint indices[6] = {0, 1, 2, 2, 3, 1};

            for (GLuint i = 0; i < (GLuint)max_sprites; ++i)
                for (GLuint j = 0; j < 6; ++j)
                    ((GLuint*)ebo_data)[i * 6 + j] = indices[j] + i * 4;
        }
            break;
        default:
            assert(0);
    }

#if SB_USE_MAPPED_VBOS
    glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
#else
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, max_verts * single_index_size,
        ebo_data, SB_BUFFER_USAGE);
    free(ebo_data);
#endif

    gls_bind_ebo(0);
    gls_bind_vbo(0);

    _sb.max_cmds    = max_sprites;
    _sb.max_floats  = max_sprites * SB_NUM_FLOATS_PER_QUAD;
    _sb.buf_size    = max_sprites * SIZE_OF_SPRITE;

    return 0;
}

void sb_destroy()
{
    glDeleteBuffers(1, &_sb.vbo);
    glDeleteBuffers(1, &_sb.ebo);
    free(_sb.cmds);
#if !SB_USE_MAPPED_VBOS
    free(_sb.mapped_vbo);
#endif
    glDeleteProgram(_sb_opaq_shader);
    glDeleteProgram(_sb_blend_shader);
    glDeleteProgram(_gui_shader);
    memset(&_sb, 0, sizeof(spritebatch_t));
}

int sb_begin(GLuint shader, float *coord_space)
{
    assert(!_sb.began);
    _sb.began = 1;

    _sb.num_cmds         = 0;
    _sb.repeat_count     = 0;

    _sb.cmds[0].num_repeats = 0;
    _sb.num_floats          = 0;

    if (coord_space) {
        _sb.coord_space[0] = coord_space[0];
        _sb.coord_space[1] = coord_space[1];
        _sb.coord_space[2] = coord_space[2];
        _sb.coord_space[3] = coord_space[3];
    } else {
        GLint vp[4];
        glGetIntegerv(GL_VIEWPORT, vp);
        _sb.coord_space[0] = 0.f;
        _sb.coord_space[1] = 0.f;
        _sb.coord_space[2] = (GLfloat)vp[2];
        _sb.coord_space[3] = (GLfloat)vp[3];
    }

    _sb.shader = shader ? shader : _sb_opaq_shader;

#if SB_USE_MAPPED_VBOS
    gls_bind_vbo(_sb.vbo);

    GLint map_flags =
        GL_MAP_WRITE_BIT;
        /* GL_MAP_UNSYNCHRONIZED_BIT; */
        /*| GL_MAP_INVALIDATE_RANGE_BIT;*/

    _sb.mapped_vbo = (GLfloat*)glMapBufferRange(GL_ARRAY_BUFFER,
        0, _sb.max_floats * sizeof(GLfloat), map_flags);
#endif

    return 0;
}

#if SB_USE_SAFE_LIMITS
#   define SB_CHECK_SPACE_FOR_ONE() \
        if (_sb.num_floats >= _sb.max_floats) \
            return;
#else
#   define SB_CHECK_SPACE_FOR_ONE()
#endif

#define SB_WRITE_SPRITE_VERTS(tx, vbo_mem, start_index, clip, x, y, z, w, h, \
    col) \
{ \
    (vbo_mem)[(start_index) +  0] = (x); \
    (vbo_mem)[(start_index) +  1] = (y); \
    (vbo_mem)[(start_index) +  2] = (z); \
    (vbo_mem)[(start_index) +  6] = (x) + (w); \
    (vbo_mem)[(start_index) +  7] = (y); \
    (vbo_mem)[(start_index) +  8] = (z); \
    (vbo_mem)[(start_index) + 12] = (x); \
    (vbo_mem)[(start_index) + 13] = (y) + ((h)); \
    (vbo_mem)[(start_index) + 14] = (z); \
    (vbo_mem)[(start_index) + 18] = (x) + (w); \
    (vbo_mem)[(start_index) + 19] = (y) + ((h)); \
    (vbo_mem)[(start_index) + 20] = (z); \
    (vbo_mem)[(start_index) +  3] = (clip)[0] / (tx)->w; \
    (vbo_mem)[(start_index) +  4] = (clip)[1] / (tx)->h; \
    (vbo_mem)[(start_index) +  9] = (clip)[2] / (tx)->w; \
    (vbo_mem)[(start_index) + 10] = (clip)[1] / (tx)->h; \
    (vbo_mem)[(start_index) + 15] = (clip)[0] / (tx)->w; \
    (vbo_mem)[(start_index) + 16] = (clip)[3] / (tx)->h; \
    (vbo_mem)[(start_index) + 21] = (clip)[2] / (tx)->w; \
    (vbo_mem)[(start_index) + 22] = (clip)[3] / (tx)->h; \
    int csi_ = (start_index) * 4; \
    ((GLubyte*)(vbo_mem))[csi_ + 20] = (col)[0]; \
    ((GLubyte*)(vbo_mem))[csi_ + 21] = (col)[1]; \
    ((GLubyte*)(vbo_mem))[csi_ + 22] = (col)[2]; \
    ((GLubyte*)(vbo_mem))[csi_ + 23] = (col)[3]; \
    ((GLubyte*)(vbo_mem))[csi_ + 44] = (col)[0]; \
    ((GLubyte*)(vbo_mem))[csi_ + 45] = (col)[1]; \
    ((GLubyte*)(vbo_mem))[csi_ + 46] = (col)[2]; \
    ((GLubyte*)(vbo_mem))[csi_ + 47] = (col)[3]; \
    ((GLubyte*)(vbo_mem))[csi_ + 68] = (col)[0]; \
    ((GLubyte*)(vbo_mem))[csi_ + 69] = (col)[1]; \
    ((GLubyte*)(vbo_mem))[csi_ + 70] = (col)[2]; \
    ((GLubyte*)(vbo_mem))[csi_ + 71] = (col)[3]; \
    ((GLubyte*)(vbo_mem))[csi_ + 92] = (col)[0]; \
    ((GLubyte*)(vbo_mem))[csi_ + 93] = (col)[1]; \
    ((GLubyte*)(vbo_mem))[csi_ + 94] = (col)[2]; \
    ((GLubyte*)(vbo_mem))[csi_ + 95] = (col)[3]; \
}

#define SB_WRITE_FLIPPED_SPRITE_VERTS(tex, vbo_mem, start_index, clip, x, y, \
    z, w, h, col, flip) \
{ \
    (vbo_mem)[(start_index) +  0] = (x); \
    (vbo_mem)[(start_index) +  1] = (y); \
    (vbo_mem)[(start_index) +  2] = (z); \
    (vbo_mem)[(start_index) +  6] = (x) + (w); \
    (vbo_mem)[(start_index) +  7] = (y); \
    (vbo_mem)[(start_index) +  8] = (z); \
    (vbo_mem)[(start_index) + 12] = (x); \
    (vbo_mem)[(start_index) + 13] = (y) + ((h)); \
    (vbo_mem)[(start_index) + 14] = (z); \
    (vbo_mem)[(start_index) + 18] = (x) + (w); \
    (vbo_mem)[(start_index) + 19] = (y) + ((h)); \
    (vbo_mem)[(start_index) + 20] = (z); \
    switch (flip) \
    { \
        case SB_FLIP_NONE: \
            (vbo_mem)[(start_index) +  3] = (clip)[0] / (tex)->w; \
            (vbo_mem)[(start_index) +  4] = (clip)[1] / (tex)->h; \
            (vbo_mem)[(start_index) +  9] = (clip)[2] / (tex)->w; \
            (vbo_mem)[(start_index) + 10] = (clip)[1] / (tex)->h; \
            (vbo_mem)[(start_index) + 15] = (clip)[0] / (tex)->w; \
            (vbo_mem)[(start_index) + 16] = (clip)[3] / (tex)->h; \
            (vbo_mem)[(start_index) + 21] = (clip)[2] / (tex)->w; \
            (vbo_mem)[(start_index) + 22] = (clip)[3] / (tex)->h; \
            break; \
        case SB_FLIP_H: \
            (vbo_mem)[(start_index) +  3] = (clip)[2] / (tex)->w;\
            (vbo_mem)[(start_index) +  4] = (clip)[1] / (tex)->h; \
            (vbo_mem)[(start_index) +  9] = (clip)[0] / (tex)->w; \
            (vbo_mem)[(start_index) + 10] = (clip)[1] / (tex)->h; \
            (vbo_mem)[(start_index) + 15] = (clip)[2] / (tex)->w;\
            (vbo_mem)[(start_index) + 16] = (clip)[3] / (tex)->h; \
            (vbo_mem)[(start_index) + 21] = (clip)[0] / (tex)->w; \
            (vbo_mem)[(start_index) + 22] = (clip)[3] / (tex)->h; \
            break; \
        case SB_FLIP_V: \
            (vbo_mem)[(start_index) +  3] = (clip)[0] / (tex)->w; \
            (vbo_mem)[(start_index) +  4] = (clip)[3] / (tex)->h;\
            (vbo_mem)[(start_index) +  9] = (clip)[2] / (tex)->w; \
            (vbo_mem)[(start_index) + 10] = (clip)[3] / (tex)->h;\
            (vbo_mem)[(start_index) + 15] = (clip)[0] / (tex)->w; \
            (vbo_mem)[(start_index) + 16] = (clip)[1] / (tex)->h; \
            (vbo_mem)[(start_index) + 21] = (clip)[2] / (tex)->w; \
            (vbo_mem)[(start_index) + 22] = (clip)[1] / (tex)->h; \
            break; \
        case SB_FLIP_BOTH: \
            (vbo_mem)[(start_index) +  3] = (clip)[2] / (tex)->w;\
            (vbo_mem)[(start_index) +  4] = (clip)[3] / (tex)->h;\
            (vbo_mem)[(start_index) +  9] = (clip)[0] / (tex)->w; \
            (vbo_mem)[(start_index) + 10] = (clip)[3] / (tex)->h; \
            (vbo_mem)[(start_index) + 15] = (clip)[2] / (tex)->w;\
            (vbo_mem)[(start_index) + 16] = (clip)[1] / (tex)->h; \
            (vbo_mem)[(start_index) + 21] = (clip)[0] / (tex)->w; \
            (vbo_mem)[(start_index) + 22] = (clip)[1] / (tex)->h; \
            break; \
    } \
    int csi_ = (start_index) * 4; \
    ((GLubyte*)(vbo_mem))[csi_ + 20] = (col)[0]; \
    ((GLubyte*)(vbo_mem))[csi_ + 21] = (col)[1]; \
    ((GLubyte*)(vbo_mem))[csi_ + 22] = (col)[2]; \
    ((GLubyte*)(vbo_mem))[csi_ + 23] = (col)[3]; \
    ((GLubyte*)(vbo_mem))[csi_ + 44] = (col)[0]; \
    ((GLubyte*)(vbo_mem))[csi_ + 45] = (col)[1]; \
    ((GLubyte*)(vbo_mem))[csi_ + 46] = (col)[2]; \
    ((GLubyte*)(vbo_mem))[csi_ + 47] = (col)[3]; \
    ((GLubyte*)(vbo_mem))[csi_ + 68] = (col)[0]; \
    ((GLubyte*)(vbo_mem))[csi_ + 69] = (col)[1]; \
    ((GLubyte*)(vbo_mem))[csi_ + 70] = (col)[2]; \
    ((GLubyte*)(vbo_mem))[csi_ + 71] = (col)[3]; \
    ((GLubyte*)(vbo_mem))[csi_ + 92] = (col)[0]; \
    ((GLubyte*)(vbo_mem))[csi_ + 93] = (col)[1]; \
    ((GLubyte*)(vbo_mem))[csi_ + 94] = (col)[2]; \
    ((GLubyte*)(vbo_mem))[csi_ + 95] = (col)[3]; \
}

#define SB_ROTATE_SPRITE(vbo_mem, first_index, angle, ox, oy) \
{ \
    float tx, ty, rx, ry; \
    for (int i = 0; i < 4; ++i) \
    { \
        tx = (vbo_mem)[(first_index) + (i * 6) + 0] - (ox); \
        ty = (vbo_mem)[(first_index) + (i * 6) + 1] - (oy); \
 \
        rx = tx * cosf((angle)) - ty * sinf((angle)); \
        ry = tx * sinf((angle)) + ty * cosf((angle)); \
 \
        (vbo_mem)[(first_index) + (i * 6) + 0] = rx + (ox); \
        (vbo_mem)[(first_index) + (i * 6) + 1] = ry + (oy); \
    } \
}

#define SB_WRITE_SPRITE_CMD(tx) \
{ \
    sb_cmd_t *cmd; \
 \
    if (_sb.num_cmds != 0) \
    { \
        cmd = &_sb.cmds[_sb.num_cmds - 1]; \
 \
        if (cmd->tex == (tx)->id) \
        { \
            ++cmd->num_repeats; \
        } else \
        { \
            cmd = &_sb.cmds[_sb.num_cmds]; \
            cmd->tex = (tx)->id; \
            cmd->num_repeats = 1; \
            ++_sb.num_cmds; \
        } \
    } else \
    { \
        cmd = &_sb.cmds[0]; \
        cmd->num_repeats = 1; \
        cmd->tex = (tx)->id; \
        ++_sb.num_cmds; \
    } \
 \
    _sb.num_floats += SB_NUM_FLOATS_PER_QUAD; \
}

void sb_sprite(tex_t *tex, float *clip, float x, float y, float z)
{
    if (_sb.num_floats >= _sb.max_floats)
        DEBUG_PRINTF("VITTU EBIIIN\n");
    SB_CHECK_SPACE_FOR_ONE();
    float w = clip[2] - clip[0];
    float h = clip[3] - clip[1];
    SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x, y, z,
        w, h, _sb_col_white);
    SB_WRITE_SPRITE_CMD(tex);
}

#define SB_SPRITE(tx, clip, x, y, z) \
    float w = clip[2] - clip[0]; \
    float h = clip[3] - clip[1]; \
    SB_WRITE_SPRITE_VERTS(tx, _sb.mapped_vbo, _sb.num_floats, clip, x, y, \
        z, w, h, _sb_col_white); \
    SB_WRITE_SPRITE_CMD(tx);
/* Inlined version of the above function */

void sb_sprite_s(tex_t *tex, float *clip, float x, float y, float z,
    float sx, float sy)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = (clip[2] - clip[0]) * sx;
    float h = (clip[3] - clip[1]) * sy;
    SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x, y, z,
        w, h, _sb_col_white);
    SB_WRITE_SPRITE_CMD(tex);
}

void sb_sprite_c(tex_t *tex, float *clip, float x, float y, float z, uint8 *col)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = clip[2] - clip[0];
    float h = clip[3] - clip[1];
    SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x, y, z,
        w, h, col);
    SB_WRITE_SPRITE_CMD(tex);
}

void sb_sprite_r(tex_t *tex, float *clip, float x, float y, float z,
    float angle, float origin_x, float origin_y)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = clip[2] - clip[0];
    float h = clip[3] - clip[1];
    float ox = origin_x + x;
    float oy = origin_y + y;
    SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x, y, z,
        w, h, _sb_col_white);
    SB_ROTATE_SPRITE(_sb.mapped_vbo, _sb.num_floats, angle, ox, oy);
    SB_WRITE_SPRITE_CMD(tex);
}

void sb_sprite_f(tex_t *tex, float *clip, float x, float y, float z,
    enum sb_flip_t flip)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = clip[2] - clip[0];
    float h = clip[3] - clip[1];
    SB_WRITE_FLIPPED_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x,
        y, z, w, h, _sb_col_white, flip);
    SB_WRITE_SPRITE_CMD(tex);
}

void sb_sprite_sc(tex_t *tex, float *clip, float x, float y, float z,
    float sx, float sy, uint8 *col)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = (clip[2] - clip[0]) * sx;
    float h = (clip[3] - clip[1]) * sy;
    SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x, y, z,
        w, h, col);
    SB_WRITE_SPRITE_CMD(tex);
}

void sb_sprite_sr(tex_t *tex, float *clip, float x, float y, float z,
    float sx, float sy, float angle, float origin_x, float origin_y)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = (clip[2] - clip[0]) * sx;
    float h = (clip[3] - clip[1]) * sy;
    float ox = origin_x + x;
    float oy = origin_y + y;
    SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x, y, z,
        w, h, _sb_col_white);
    SB_ROTATE_SPRITE(_sb.mapped_vbo, _sb.num_floats, angle, ox, oy);
    SB_WRITE_SPRITE_CMD(tex);
}

void sb_sprite_sf(tex_t *tex, float *clip, float x, float y, float z,
    float sx, float sy, enum sb_flip_t flip)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = (clip[2] - clip[0]) * sx;
    float h = (clip[3] - clip[1]) * sy;
    SB_WRITE_FLIPPED_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip,
        x, y, z, w, h, _sb_col_white, flip);
    SB_WRITE_SPRITE_CMD(tex);
}

void sb_sprite_cr(tex_t *tex, float *clip, float x, float y, float z,
    uint8 *col, float angle, float origin_x, float origin_y)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = clip[2] - clip[0];
    float h = clip[3] - clip[1];
    float ox = origin_x + x;
    float oy = origin_y + y;
    SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x, y, z,
        w, h, col);
    SB_ROTATE_SPRITE(_sb.mapped_vbo, _sb.num_floats, angle, ox, oy);
    SB_WRITE_SPRITE_CMD(tex);
}

void sb_sprite_cf(tex_t *tex, float *clip, float x, float y, float z,
    uint8 *col, enum sb_flip_t flip)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = clip[2] - clip[0];
    float h = clip[3] - clip[1];
    SB_WRITE_FLIPPED_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x,
        y, z, w, h, col, flip);
    SB_WRITE_SPRITE_CMD(tex);
}

void sb_sprite_scr(tex_t *tex, float *clip, float x, float y, float z,
    float sx, float sy, uint8 *col, float angle, float origin_x, float origin_y)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = (clip[2] - clip[0]) * sx;
    float h = (clip[3] - clip[1]) * sy;
    float ox = origin_x + x;
    float oy = origin_y + y;
    SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x, y, z,
        w, h, col);
    SB_ROTATE_SPRITE(_sb.mapped_vbo, _sb.num_floats, angle, ox, oy);
    SB_WRITE_SPRITE_CMD(tex);
}

void sb_sprite_scf(tex_t *tex, float *clip, float x, float y, float z,
    float sx, float sy, uint8 *col, enum sb_flip_t flip)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = (clip[2] - clip[0]) * sx;
    float h = (clip[3] - clip[1]) * sy;
    SB_WRITE_FLIPPED_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip,
        x, y, z, w, h, col, flip);
    SB_WRITE_SPRITE_CMD(tex);
}

void sb_sprite_scrf(tex_t *tex, float *clip, float x, float y, float z,
    float sx, float sy, uint8 *col, float angle, float origin_x, float origin_y,
    enum sb_flip_t flip)
{
    SB_CHECK_SPACE_FOR_ONE();
    float w = (clip[2] - clip[0]) * sx;
    float h = (clip[3] - clip[1]) * sy;
    float ox = origin_x + x;
    float oy = origin_y + y;
    SB_WRITE_FLIPPED_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, clip, x,
        y, z, w, h, col, flip);
    SB_ROTATE_SPRITE(_sb.mapped_vbo, _sb.num_floats, angle, ox, oy);
    SB_WRITE_SPRITE_CMD(tex);
}

#define SB_PREP_SPRITE_CMD_FOR_TEXT(cmd_ptr, tex) \
{ \
    if (_sb.num_cmds != 0) \
    { \
        cmd_ptr = &_sb.cmds[_sb.num_cmds - 1]; \
 \
        if (cmd_ptr->tex != (tex)->id) \
        { \
            (cmd_ptr) = &_sb.cmds[_sb.num_cmds]; \
            (cmd_ptr)->tex = (tex)->id; \
            (cmd_ptr)->num_repeats = 0; \
            ++_sb.num_cmds; \
        } \
    } \
    else \
    { \
        cmd_ptr = &_sb.cmds[0]; \
        cmd_ptr->tex = tex->id; \
        cmd_ptr->num_repeats = 0; \
        ++_sb.num_cmds; \
    } \
}

#define SB_WRITE_TEXT(sf, txt, x, y, z)

void sb_text(sfont_t *sf, const char *txt, float x, float y, float z)
{
    SB_CHECK_SPACE_FOR_ONE();

    if (!txt || !txt[0] || !sfont_is_loaded(sf))
        return;

    tex_t *tex = &sf->tex;
    sfont_glyph_t *g;
    float w, h, rx, ry;
    float px = x;
    float py = y;

    sb_cmd_t *cmd;
    SB_PREP_SPRITE_CMD_FOR_TEXT(cmd, tex);

    for (const char *c = txt; *c; ++c) {
        SB_CHECK_SPACE_FOR_ONE();

        if (*c == '\n') {
            px = x;
            py += sf->v_advance;
            continue;
        }

        g   = SFONT_GLYPH(sf, (int)*c);
        w   = g->clip[2] - g->clip[0];
        h   = g->clip[3] - g->clip[1];
        rx  = px;
        ry  = (py - h + g->y_off);

        SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, g->clip, rx,
            ry, z, w, h, _sb_col_white);

        px += g->advance;
        ++cmd->num_repeats;
        _sb.num_floats += SB_NUM_FLOATS_PER_QUAD;
    }
}

void sb_text_sc(sfont_t *sf, const char *txt, float x, float y, float z,
    float sx, float sy, uint8 *col)
{
    SB_CHECK_SPACE_FOR_ONE();

    if (!txt || !txt[0] || !sfont_is_loaded(sf))
        return;

    tex_t *tex = &sf->tex;
    sfont_glyph_t *g;
    float w, h, rx, ry;
    float px = x;
    float py = y;

    sb_cmd_t *cmd;
    SB_PREP_SPRITE_CMD_FOR_TEXT(cmd, tex);

    for (const char *c = txt; *c; ++c) {
        SB_CHECK_SPACE_FOR_ONE();

        if (*c == '\n') {
            px = x;
            py += (sf->v_advance * sy);
            continue;
        }

        g   = SFONT_GLYPH(sf, (int)*c);
        w   = (g->clip[2] - g->clip[0]) * sx;
        h   = (g->clip[3] - g->clip[1]) * sy;
        rx  = px;
        ry  = (py - h + g->y_off);

        SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, g->clip, rx,
            ry, z, w, h, col);

        px += (g->advance * sx);
        ++cmd->num_repeats;
        _sb.num_floats += SB_NUM_FLOATS_PER_QUAD;
    }
}

void sb_text_wrap(sfont_t *sf, const char *txt, float x, float y, float z,
    float wrap)
{
    SB_CHECK_SPACE_FOR_ONE();

    if (!txt || !txt[0] || !sfont_is_loaded(sf))
        return;

    tex_t *tex = &sf->tex;
    sfont_glyph_t *g;
    float w, h, rx, ry;
    float px    = x;
    float py    = y;
    float max_w = x + wrap;

    sb_cmd_t *cmd;
    SB_PREP_SPRITE_CMD_FOR_TEXT(cmd, tex);

    for (const char *c = txt; *c; ++c) {
        SB_CHECK_SPACE_FOR_ONE();

        if (*c == '\n') {
            px = x;
            py += sf->v_advance;
            continue;
        }

        g   = SFONT_GLYPH(sf, (int)*c);
        w   = g->clip[2] - g->clip[0];
        h   = g->clip[3] - g->clip[1];

        if (c != txt && px + g->advance > max_w ) {
            px = x;
            py += sf->v_advance;
        }

        rx  = px;
        ry  = (py - h + g->y_off);

        SB_WRITE_SPRITE_VERTS(tex, _sb.mapped_vbo, _sb.num_floats, g->clip, rx,
            ry, z, w, h, _sb_col_white);

        px += g->advance;
        ++cmd->num_repeats;
        _sb.num_floats += SB_NUM_FLOATS_PER_QUAD;
    }
}

#define SB_WRITE_WRAPPED_STRING_VERTS_S(sb, sf, txt, x, y, z, wrap, sx, sy, col) \
{ \
    sfont_glyph_t *g; \
    float w, h, rx, ry; \
    float px    = x; \
    float py    = y; \
    float max_w = x + wrap; \
    float x_adv; \
    float y_adv = sf->v_advance * sx; \
\
    for (const char *c = txt; *c; ++c) \
    { \
        SB_CHECK_SPACE_FOR_ONE(); \
\
        if (*c == '\n') \
        { \
            px = x; \
            py += y_adv; \
            continue; \
        } \
 \
        g       = SFONT_GLYPH(sf, (int)*c); \
        w       = (g->clip[2] - g->clip[0]) * sx; \
        h       = (g->clip[3] - g->clip[1]) * sy; \
        x_adv   = g->advance * sx; \
 \
        if (c != txt && px + x_adv > max_w ) \
        { \
            px = x; \
            py += y_adv; \
        } \
 \
        rx  = px; \
        ry  = (py - h + (sy * g->y_off)); \
 \
        SB_WRITE_SPRITE_VERTS(tex, sb.mapped_vbo, sb.num_floats, g->clip, rx, \
            ry, z, w, h, col); \
 \
        px += x_adv; \
        ++cmd->num_repeats; \
        sb.num_floats += SB_NUM_FLOATS_PER_QUAD; \
    } \
}


void sb_text_wrap_s(sfont_t *sf, const char *txt, float x, float y, float z,
    float wrap, float sx, float sy)
{
    SB_CHECK_SPACE_FOR_ONE(); \

    if (!txt || !txt[0] || !sfont_is_loaded(sf))
        return;

    tex_t *tex = &sf->tex;

    sb_cmd_t *cmd;
    SB_PREP_SPRITE_CMD_FOR_TEXT(cmd, tex);
    SB_WRITE_WRAPPED_STRING_VERTS_S(_sb, sf, txt, x, y, z, wrap, sx, sy,
        _sb_col_white);
}

void sb_text_wrap_sc(sfont_t *sf, const char *txt, float x, float y, float z,
    float wrap, float sx, float sy, uint8 *col)
{
    SB_CHECK_SPACE_FOR_ONE(); \

    if (!txt || !txt[0] || !sfont_is_loaded(sf))
        return;

    tex_t *tex = &sf->tex;

    sb_cmd_t *cmd;
    SB_PREP_SPRITE_CMD_FOR_TEXT(cmd, tex);
    SB_WRITE_WRAPPED_STRING_VERTS_S(_sb, sf, txt, x, y, z, wrap, sx, sy, col);
}

int
sb_end()
{
    assert(_sb.began);
    _sb.began = 0;

#if SB_USE_MAPPED_VBOS
    glUnmapBuffer(GL_ARRAY_BUFFER);
#else
    gls_bind_vbo(_sb.vbo);
    glBufferSubData(GL_ARRAY_BUFFER, 0, _sb.num_floats * sizeof(GLfloat),
        _sb.mapped_vbo);
#endif

    gls_bind_ebo(_sb.ebo);
    _bind_sb_vao();

    const float *cs = _sb.coord_space;

    GLfloat projection[4][4] = {
        {2.0f / (cs[2] - cs[0]), 0.0f, 0.0f, 0.0f},
        {0.0f, 2.0f / (cs[1] - cs[3]), 0.0f, 0.0f},
        {0.0f, 0.0f, -1.0f, 0.0f},
        {- (cs[2] + cs[0]) / (cs[2] - cs[0]),
         - (cs[1] + cs[3]) / (cs[1] - cs[3]),
         0.0f, 1.0f}
    };

    if (gl_state.bound_program != _sb.shader)
        gls_use_program(_sb.shader);
    gls_active_texture(GL_TEXTURE0);

    glUniformMatrix4fv(glGetUniformLocation(_sb_opaq_shader, "projection"),
        1, GL_FALSE, &projection[0][0]);

    GLsizei index_size;

    switch (_sb.index_type) {
        case GL_UNSIGNED_BYTE:  index_size = sizeof(GLubyte);   break;
        case GL_UNSIGNED_SHORT: index_size = sizeof(GLushort);  break;
        case GL_UNSIGNED_INT:   index_size = sizeof(GLuint);    break;
        default: assert(0); break;
    }

    sb_cmd_t            *cmd;
    long unsigned int   offset      = 0;
    const GLenum        index_type  = _sb.index_type;
    int                 num_cmds    = _sb.num_cmds;

    for (int i = 0; i < num_cmds; ++i) {
        cmd = &_sb.cmds[i];
        gls_bind_tex2d_if_not_bound(cmd->tex);
        glDrawElements(GL_TRIANGLES, cmd->num_repeats * 6, index_type,
            (const GLvoid*)offset);
        offset += cmd->num_repeats * 6 * index_size;
    }

#if SB_USE_MAPPED_VBOS
    /* Orphan the buffer */
    glMapBufferRange(GL_ARRAY_BUFFER,
        0, _sb.max_floats * sizeof(GLfloat), GL_MAP_INVALIDATE_BUFFER_BIT);
#endif

    gls_bind_vbo(0);
    gls_bind_ebo(0);
    return 0;
}

int
r_init(void *sdl_window)
{
    char        err_log[512];
    const int   err_log_len = 512;

    if (init_graphics_api(sdl_window))
        return 1;

    _init_gl_state();

    int r;
    if ((r = sb_init(65385, err_log, err_log_len))) {
        DEBUG_PRINTFF("sb_init() returned %d, GL error log: %s\n", r, err_log);
        return 4;
    }

    /* Init the gui rendering stuff */
    {
    const char *vtx_code =
        "#version 100\n"
        "precision lowp float;"
        "attribute vec2 position;"
        "attribute vec2 in_uv;"
        "attribute vec4 in_color;"
        "varying vec2 uv;"
        "varying vec4 color;"
        "uniform mat4 projection;"
        "void main(void)"
        "{"
            "gl_Position = projection * vec4(position, 1.0, 1.0);"
            "uv = in_uv;"
            "color = in_color;"
        "}";

    const char *frg_code =
        "#version 100\n"
        "precision lowp float;"
        "varying vec4 color;"
        "varying vec2 uv;"
        "uniform sampler2D tex;"
        "void main(void)\n"
        "{"
            "gl_FragColor = texture2D(tex, uv) * color;"
        "}";

    _gui_shader = shader_from_strs(vtx_code, frg_code, 0, 0);

    if (glGetError())
        return 5;
    }

    /* Create the while dummy texture */
    uint8 white_tex_px[16];

    for (int i = 0; i < 16; ++i)
        white_tex_px[i] = 255;

    if (tex_from_mem(&_white_dummy_tex, white_tex_px, 2, 2, IMG_RGBA) != 0)
        return 6;

    /* Create the GUI rendering buffers */
    const uint num_quads = 16384; /* TODO: fix the hardcoded value */
    DEBUG_PRINTFF("gui num quads is hardcoded FIXME\n");
    const uint num_verts = num_quads * 6;
    _gui_vbo_size = num_verts * GUI_VERT_SIZE;

    glGenBuffers(1, &_gui_vbo);
    glGenBuffers(1, &_gui_ebo);

    /* VBO */
    gls_bind_vbo(_gui_vbo);
    glBufferData(GL_ARRAY_BUFFER, _gui_vbo_size, 0, GL_DYNAMIC_DRAW);

    _bind_gui_vao();

    /* EBO */
    gls_bind_ebo(_gui_ebo);
#if SB_USE_MAPPED_VBOS
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, num_verts / 4 * 6 * sizeof(GLushort),
        0, GL_STREAM_DRAW);
    GLushort *ebo_data = (GLushort*)glMapBuffer(GL_ELEMENT_ARRAY_BUFFER,
        GL_WRITE_ONLY);
#else
    GLushort *ebo_data = calloc(num_verts / 4 * 6, sizeof(GLushort));
#endif

    GLushort indices[6] = {0, 1, 2, 2, 3, 1};
    for (GLushort i = 0; i < num_quads; ++i)
        for (GLushort j = 0; j < 6; ++j)
            ebo_data[i * 6 + j] = indices[j] + i * 4;

    glBufferData(GL_ELEMENT_ARRAY_BUFFER, num_verts / 4 * 6 * sizeof(GLushort),
        ebo_data, GL_STREAM_DRAW);

#if SB_USE_MAPPED_VBOS
    glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
#else
    free(ebo_data);
#endif
    gls_bind_ebo(0);
    gls_bind_vbo(0);

    r_set_vsync(1);
    return 0;
}

int
r_destroy()
{
    sb_destroy();
    tex_free(&_white_dummy_tex);
    glDeleteBuffers(1, &_gui_vbo);
    glDeleteBuffers(1, &_gui_ebo);
    SDL_GL_DeleteContext(_gl_context);
    return 0;
}

void
r_render_gui(gui_draw_list_t *lists, int num_lists)
{
    gui_draw_cmd_t  *cmd;
    gui_draw_list_t *list;

    if (gl_state.depth_test_enabled)
        gls_disable_depth_test();

    int prev_vp[4], prev_scis[4];
    r_get_viewport(&prev_vp[0], &prev_vp[1], &prev_vp[2], &prev_vp[3]);
    r_get_scissor(&prev_scis[0], &prev_scis[1], &prev_scis[2], &prev_scis[3]);

    int tar_vp[4];
    core_compute_target_viewport(tar_vp);
    r_viewport(tar_vp[0], tar_vp[1], tar_vp[2], tar_vp[3]);
    r_scissor(tar_vp[0], tar_vp[1], tar_vp[2], tar_vp[3]);

    gls_use_program(_gui_shader);

    GLfloat cs[4] = {0.f, 0.f, (float)RESOLUTION_W, (float)RESOLUTION_H};

    GLfloat projection[4][4] = {
        {2.0f / (cs[2] - cs[0]), 0.0f, 0.0f, 0.0f},
        {0.0f, 2.0f / (cs[1] - cs[3]), 0.0f, 0.0f},
        {0.0f, 0.0f, -1.0f, 0.0f},
        {- (cs[2] + cs[0]) / (cs[2] - cs[0]),
         - (cs[1] + cs[3]) / (cs[1] - cs[3]),
         0.0f, 1.0f}
    };

    glUniformMatrix4fv(glGetUniformLocation(_gui_shader, "projection"),
        1, GL_FALSE, &projection[0][0]);

    gls_bind_vbo(_gui_vbo);
    gls_bind_ebo(_gui_ebo);
    _bind_gui_vao();

    gls_active_texture(GL_TEXTURE0);
    gls_bind_tex2d(_white_dummy_tex.id);
    GLuint next_tex;

    int i, j, num_cmds;
    long offset;
    GLint num_elements;
    int vp[4];
    int clip[4];
    float sc = core_compute_target_viewport(vp);
    sc = MAX(sc, 0.00000001f);
    for (i = 0; i < num_lists; ++i) {
        /* TODO: resize buf if necessary */
        list        = &lists[i];
        offset      = 0;
        num_cmds    = list->num_cmds;
        clip[0]     = (int)((float)list->clip[0] * sc) + vp[0];
        clip[1]     = (int)((float)list->clip[1] * sc) + vp[1];
        clip[2]     = (int)((float)list->clip[2] * sc);
        clip[3]     = (int)((float)list->clip[3] * sc);

        r_scissor(clip[0], clip[1], clip[2], clip[3]);
        glBufferSubData(GL_ARRAY_BUFFER, 0,
            list->num_verts * GUI_VERT_SIZE, list->verts);

        for (j = 0; j < num_cmds; ++j) {
            cmd = &list->cmds[j];
            next_tex = cmd->tex ? cmd->tex : _white_dummy_tex.id;
            num_elements = cmd->num_verts / 4 * 6;
            gls_bind_tex2d_if_not_bound(next_tex);
            glDrawElements(GL_TRIANGLES, num_elements, GL_UNSIGNED_SHORT,
                (const GLvoid*)(offset));
            offset += num_elements * sizeof(GLushort);
        }
    }

    glBufferData(GL_ARRAY_BUFFER, _gui_vbo_size, 0, GL_DYNAMIC_DRAW);
    gls_bind_ebo(0);
    gls_bind_vbo(0);
    gls_use_program(0);
    r_viewport(prev_vp[0], prev_vp[1], prev_vp[2], prev_vp[3]);
    r_scissor(prev_scis[0], prev_scis[1], prev_scis[2], prev_scis[3]);
}

void
r_color(float r, float g, float b, float a)
    {glClearColor(r, g, b, a);}

void
r_clear(int mask)
    {glClear(mask);}

void
r_scissor(int x, int y, int w, int h)
{
    glScissor(x, main_window_h - h - y, w, h);
    gl_state.scissor[0] = x;
    gl_state.scissor[1] = y;
    gl_state.scissor[2] = w;
    gl_state.scissor[3] = h;
}

void
r_get_scissor(int *x, int *y, int *w, int *h)
{
    *x = gl_state.scissor[0];
    *y = gl_state.scissor[1];
    *w = gl_state.scissor[2];
    *h = gl_state.scissor[3];
}

void
r_viewport(int x, int y, int w, int h)
{
    glViewport(x, main_window_h - h - y, w, h);
    gl_state.viewport[0] = x;
    gl_state.viewport[1] = y;
    gl_state.viewport[2] = w;
    gl_state.viewport[3] = h;
}

void
r_get_viewport(int *x, int *y, int *w, int *h)
{
    *x = gl_state.viewport[0];
    *y = gl_state.viewport[1];
    *w = gl_state.viewport[2];
    *h = gl_state.viewport[3];
}

void
r_set_vsync(bool32 opt)
    {SDL_GL_SetSwapInterval(opt);}

bool32
r_get_vsync()
    {return SDL_GL_GetSwapInterval();}

void
r_swap_buffers(void *sdl_window)
{
    SDL_GL_SwapWindow((SDL_Window*)sdl_window);
}

static inline GLenum
_img_pxf_to_gl_enum(enum img_pxf_t pxf)
{
    switch (pxf)
    {
        case IMG_RGB:   return GL_RGB;
        case IMG_RGBA:  return GL_RGBA;
        case IMG_ALPHA: return GL_ALPHA;
        default: assert(0); return 0;
    }
}

static void
_init_gl_state()
{
    gl_state.bound_tex2ds = _bound_tex2ds;
    gls_active_texture(GL_TEXTURE0);
    gls_enable_blend();
    gls_blend_func(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    gls_disable_depth_test();
    gls_enable_scissor();
    glGetIntegerv(GL_VIEWPORT, gl_state.viewport);
    glViewport(0, 0, main_window_w, main_window_h);
    glScissor(0, 0, main_window_w, main_window_h);
    gl_state.viewport[0]    = 0;
    gl_state.viewport[1]    = 0;
    gl_state.viewport[2]    = main_window_w;
    gl_state.viewport[3]    = main_window_h;
    gl_state.scissor[0]     = 0;
    gl_state.scissor[1]     = 0;
    gl_state.scissor[2]     = main_window_w;
    gl_state.scissor[3]     = main_window_h;
}

static inline void
_bind_gui_vao()
{
    glVertexAttribPointer(SHADER_ATTR_LOC_POS, 2, GL_FLOAT, GL_FALSE,
        5 * sizeof(GLfloat), 0);
    glVertexAttribPointer(SHADER_ATTR_LOC_UV, 2, GL_FLOAT, GL_FALSE,
        5 * sizeof(GLfloat), (GLvoid*)(2 * sizeof(GLfloat)));
    glVertexAttribPointer(SHADER_ATTR_LOC_COL, 4, GL_UNSIGNED_BYTE, GL_TRUE,
        5 * sizeof(GLfloat), (GLvoid*)(4 * sizeof(GLfloat)));
    glEnableVertexAttribArray(SHADER_ATTR_LOC_UV);
    glEnableVertexAttribArray(SHADER_ATTR_LOC_POS);
    glEnableVertexAttribArray(SHADER_ATTR_LOC_COL);
}

static inline void
_bind_sb_vao()
{
    glVertexAttribPointer(SHADER_ATTR_LOC_POS, 3, GL_FLOAT, GL_FALSE,
        6 * sizeof(GLfloat), 0);
    glVertexAttribPointer(SHADER_ATTR_LOC_UV, 2, GL_FLOAT, GL_FALSE,
        6 * sizeof(GLfloat), (const GLvoid *)(3 * sizeof(GLfloat)));
    glVertexAttribPointer(SHADER_ATTR_LOC_COL, 4, GL_UNSIGNED_BYTE, GL_TRUE,
        6 * sizeof(GLfloat), (const GLvoid *)(5 * sizeof(GLfloat)));
    glEnableVertexAttribArray(SHADER_ATTR_LOC_UV);
    glEnableVertexAttribArray(SHADER_ATTR_LOC_POS);
    glEnableVertexAttribArray(SHADER_ATTR_LOC_COL);
}

static inline void
gls_use_program(GLuint program)
{
    glUseProgram(program);
    gl_state.bound_program = program;
}

static inline void
gls_bind_vbo(GLuint vbo)
{
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    gl_state.bound_vbo = vbo;
}

static inline void
gls_bind_ebo(GLuint ebo)
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    gl_state.bound_ebo = ebo;
}

static inline void
gls_bind_tex2d(GLuint tex)
{
    glBindTexture(GL_TEXTURE_2D, tex);
    gl_state.bound_tex2ds[GLS_ACTIVE_TEX_INDEX()] = tex;
}

static inline void
gls_bind_tex2d_if_not_bound(GLuint tex)
{
    if (gl_state.bound_tex2ds[GLS_ACTIVE_TEX_INDEX()] == tex) return;
    gls_bind_tex2d(tex);
}

static inline void
gls_active_texture(GLenum tex)
{
    glActiveTexture(tex);
    gl_state.active_texture = tex;
}

static inline void
gls_enable_blend()
{
    glEnable(GL_BLEND);
    gl_state.blend_enabled = 1;
}

static inline void
gls_disable_blend()
{
    glDisable(GL_BLEND);
    gl_state.blend_enabled = 0;
}

static inline void
gls_blend_func(GLenum sfactor, GLenum dfactor)
{
    glBlendFunc(sfactor, dfactor);
    gl_state.blend_sfactor = sfactor;
    gl_state.blend_dfactor = dfactor;
}

static inline void
gls_enable_depth_test()
{
    glEnable(GL_DEPTH_TEST);
    gl_state.depth_test_enabled = 1;
}

static inline void
gls_disable_depth_test()
{
    glDisable(GL_DEPTH_TEST);
    gl_state.depth_test_enabled = 0;
}

static inline void
gls_enable_scissor()
{
    glEnable(GL_SCISSOR_TEST);
    gl_state.scissor_enabled = 1;
}

static inline void
gls_disable_scissor()
{
    glDisable(GL_SCISSOR_TEST);
    gl_state.scissor_enabled = 0;
}
