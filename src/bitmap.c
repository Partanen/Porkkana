#include "bitmap.h"
#include "util.h"

#define STB_RECT_PACK_IMPLEMENTATION
#include "stb_rect_pack.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h"

int
img_load(img_t *img, const char *path)
{
    size_t len;
    void *buf = load_file_to_buffer(path, &len);
    if (!buf)
        return 1;
    img->pixels = stbi_load_from_memory(buf, len, &img->w, &img->h,
        &img->bytes_per_px, 0);
    free(buf);
    if (!img->pixels)
    {
        DEBUG_PRINTFF("failed for path '%s'.\n", path);
        return 2;
    }
    img->pxf = img->bytes_per_px == 3 ? IMG_RGB : IMG_RGBA;
    return 0;
}

void
img_free(img_t *img)
{
    stbi_image_free(img->pixels);
    memset(img, 0, sizeof(img_t));
}

int
ttf_load(ttf_t *font, const char *path, int px_height, int h_oversample,
    int v_oversample)
{
    uint8               *file               = 0;
    uint8               *tmp_pixels         = 0;
    stbtt_packedchar    *char_data          = 0;
    uint8               *transformed_pixels = 0;

    file = load_file_to_buffer(path, 0);
    if (!file)
        return 1;

    int ret = 0;

    stbtt_fontinfo font_info;
    if (!stbtt_InitFont(&font_info, file, 0))
        {ret = 2; goto out;}

    int num_glyphs = font_info.numGlyphs < 256 ? 256 : font_info.numGlyphs;

    /* Allocate everything */
    uint num_char_data_bytes = sizeof(stbtt_packedchar) * num_glyphs;
    char_data = calloc(1, num_char_data_bytes);
    if (!char_data)
        {ret = 3; goto out;}

    int     bmp_w = 0;
    bool32  baked = 0;

    /* Attempt to brute force into a suitable bitmap */
    for (bmp_w = 256; bmp_w < 2048; bmp_w = bmp_w * 2)
    {
        uint8 *old_tmp_pixels = tmp_pixels;
        tmp_pixels = realloc(tmp_pixels, bmp_w * bmp_w);
        if (!tmp_pixels)
        {
            free(old_tmp_pixels);
            ret = 5;
            goto out;
        }
        memset(tmp_pixels, 0, bmp_w * bmp_w);

        stbtt_pack_context context;

        if (!stbtt_PackBegin(&context, tmp_pixels, bmp_w, bmp_w, 0, 1, 0))
            continue;

        if (h_oversample > 0 || v_oversample > 0)
            stbtt_PackSetOversampling(&context, 1, 1);

        if (!stbtt_PackFontRange(&context, file, 0,
            (float)STBTT_POINT_SIZE(px_height),
            0, num_glyphs, char_data))
        {
            stbtt_PackEnd(&context);
            continue;
        }

        stbtt_PackEnd(&context);

        baked = 1;
        break;
    }

    if (!baked)
        {ret = 6; goto out;}

    transformed_pixels = calloc(1, bmp_w * bmp_w * 4);
    if (!transformed_pixels) {ret = 4; goto out;}


    for (int i = 0; i < bmp_w * bmp_w; ++i)
    {
        if (tmp_pixels[i])
        {
            transformed_pixels[i * 4 + 0] = 0xFF;
            transformed_pixels[i * 4 + 1] = 0xFF;
            transformed_pixels[i * 4 + 2] = 0xFF;
            transformed_pixels[i * 4 + 3] = tmp_pixels[i];
        } else
        {
            transformed_pixels[i * 4 + 0] = 0;
            transformed_pixels[i * 4 + 1] = 0;
            transformed_pixels[i * 4 + 2] = 0;
            transformed_pixels[i * 4 + 3] = 0;
        }
    }

    font->info          = font_info;
    font->bitmap        = transformed_pixels;
    font->bitmap_w      = bmp_w;
    font->bitmap_h      = bmp_w;
    font->glyphs        = char_data;
    font->scale         = stbtt_ScaleForPixelHeight(&font_info,
                            (float)px_height);
    font->pixel_height  = px_height;
    font->num_glyphs    = num_glyphs;
    font->file          = file;

    stbtt_GetFontVMetrics(&font_info, &font->ascent, &font->descent,
        &font->linegap);

    out:
        free(tmp_pixels);
        if (ret)
        {
            free(file);
            free(char_data);
            free(transformed_pixels);
            memset(font, 0, sizeof(ttf_t));
        }
        return ret;
}

void
ttf_free(ttf_t *font)
{
    free(font->glyphs); /* This also frees the bitmap */
    free(font->bitmap); /* This also frees the bitmap */
    free(font->file);
    memset(font, 0, sizeof(ttf_t));
}
