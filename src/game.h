#ifndef GAME_H
#define GAME_H

typedef struct screen_t screen_t;

extern screen_t *game;

void game_open();
void game_update(double dt);

#endif /* GAME_H */
