#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "assets.h"
#include "render.h"
#include "bitmap.h"
#include "util.h"

DYNAMIC_HASH_TABLE_DEFINITION(sfont_table, sfont_t*, const char*, uint64,
    fnv_hash64_from_str, 2);
DYNAMIC_HASH_TABLE_DEFINITION(tex_table, tex_t*, const char*, uint64,
    fnv_hash64_from_str, 2);
DYNAMIC_HASH_TABLE_DEFINITION(anim_table, anim_t*, const char*, uint64,
    fnv_hash64_from_str, 2);
DYNAMIC_HASH_TABLE_DEFINITION(spritesheet_table, spritesheet_t*, const char*,
    uint64, fnv_hash64_from_str, 2);

static struct {
    obj_pool_t      pool;
    sfont_table_t   table;
} _fonts;

static struct {
    obj_pool_t  pool;
    tex_table_t table;
} _texes;

static struct {
    obj_pool_t      pool;
    anim_table_t    table;
} _anims;

static struct {
    obj_pool_t          pool;
    spritesheet_table_t table;
} _spritesheets;

static int _load_fonts();
static int _load_texes();
static int _load_anims();
static int _load_spritesheets();
static int _load_anim(anim_t *an, const char *path);
static int _load_spritesheet(spritesheet_t *sheet, const char *fp);

int as_init()
{
    DEBUG_PRINTFF("loading assets\n");
    _load_fonts();
    _load_texes();
    _load_anims();
    _load_spritesheets();
    DEBUG_PRINTFF("finished loading assets\n");
    return 0;
}

sfont_t *as_get_font(const char *name)
{
    sfont_t **ret = sfont_table_get_ptr(&_fonts.table, name);
    return ret ? *ret : 0;
}

tex_t *as_get_tex(const char *name)
{
    tex_t **ret = tex_table_get_ptr(&_texes.table, name);
    return ret ? *ret : 0;
}

anim_t *as_get_anim(const char *name)
{
    anim_t **ret = anim_table_get_ptr(&_anims.table, name);
    return ret ? *ret : 0;
}

spritesheet_t *as_get_spritesheet(const char *name)
{
    spritesheet_t **ret = spritesheet_table_get_ptr(&_spritesheets.table, name);
    return ret ? *ret : 0;
}

sprite_data_t spritesheet_get_data(spritesheet_t *ss, const char *name)
{
    sprite_data_t ret = {0};
    int num_clips = ss->num_clips;
    for (int i = 0; i < num_clips; ++i) {
        if (!streq(ss->clip_names[i], name))
            continue;
         ret.tex = ss->ta;
         memcpy(ret.clip, ss->clips[i], 4 * sizeof(float));
         break;
    }
    return ret;
}

typedef struct {
    sfont_t *sf;
    int     size; /* -1 if none yet */
    int     have_path;
    dchar   *path;
} font_ctx_t;

static int _on_font_def(void *ctx, const char *opt, const char *val)
{
    if (!streq(opt, "font"))
        return 1;
    if (sfont_table_exists(&_fonts.table, val))
        return 2;
    sfont_t *sf = obj_pool_reserve(&_fonts.pool);
    memset(sf, 0, sizeof(sfont_t));
    sfont_table_insert(&_fonts.table, val, sf);

    font_ctx_t *fc  = ctx;
    fc->sf          = sf;
    fc->size        = -1;
    fc->have_path   = 0;

    return 0;
}

static int _on_font_opt(void *ctx, const char *opt, const char *val)
{
    int ret = 0;
    font_ctx_t *fc  = ctx;

    if (streq(opt, "path")) {
        dstr_set(&fc->path, val);
        fc->have_path = 1;
    } else if (streq(opt, "size"))
        fc->size = atoi(val);

    if (fc->have_path && fc->size != -1) {
        ttf_t ttf;
        if (ttf_load(&ttf, fc->path, fc->size, 2, 2))
            return 1;
        if (sfont_from_ttf(fc->sf, &ttf))
            ret = 2;
        ttf_free(&ttf);
    }
    return ret;
}

static int _load_fonts()
{
    obj_pool_init(&_fonts.pool, 8, sizeof(sfont_t));
    sfont_table_einit(&_fonts.table, 8);
    font_ctx_t ctx = {0};
    ctx.size = -1;
    return parse_def_file("fonts.def", _on_font_def, _on_font_opt, &ctx);
}

static void _on_tex_opt(void *ctx, const char *opt, const char *val)
{
    if (tex_table_exists(&_texes.table, opt))
        return;

    img_t img;
    if (img_load(&img, val)) {
        DEBUG_PRINTFF("failed img_load()\n");
        return;
    }

    tex_t *tex = obj_pool_reserve(&_texes.pool);
    int r = tex_from_img(tex, &img);
    img_free(&img);

    if (r) {
        obj_pool_free(&_texes.pool, tex);
        DEBUG_PRINTFF("failed tex_from_img()\n");
        return;
    }
    DEBUG_PRINTFF("Loaded texture '%s' from path '%s'\n", opt, val);
    tex_table_einsert(&_texes.table, opt, tex);
}

static int _load_texes()
{
    int num = 64;
    obj_pool_init(&_texes.pool, num, sizeof(tex_t));
    tex_table_einit(&_texes.table, num);
    int r = parse_cfg_file("texes.cfg", _on_tex_opt, 0);
    if (r)
        DEBUG_PRINTFF("failed to load texes.cfg: error %d\n", r);
    return r;
}

static void _on_anim_opt(void *ctx, const char *opt, const char *val)
{
    if (anim_table_exists(&_anims.table, opt))
        return;
    anim_t an;
    if (_load_anim(&an, val))
        return;
    anim_t *anim = obj_pool_reserve(&_anims.pool);
    *anim = an;
    anim_table_einsert(&_anims.table, opt, anim);
}

static int _load_anims()
{
    int num = 64;
    obj_pool_init(&_anims.pool, num, sizeof(anim_t));
    anim_table_einit(&_anims.table, num);
    return parse_cfg_file("anims.cfg", _on_anim_opt, 0);
}

static int _load_anim(anim_t *an, const char *path)
{
    anim_t *anim = an;

    file_t *fp = 0;
    fp = file_open(path, "r");

    if(!fp)
        return 1;

    int ret = 0;

    char name[128] = {0};
    file_gets(name, 128, fp);

    for (int i = 0; i < 128; ++i)
        if (name[i] == '\n') {name[i] = '\0'; break;}

    char line[1024];

    anim->num_frames = 0;

    int num_frames = -1;

    file_gets(line, 1024, fp);
    sscanf(line, "%d", &num_frames);

    if (num_frames <= 0)
        {ret = 1; goto out;}

    anim->num_frames = num_frames;
    anim_frame_t *frames_for_anim = calloc(num_frames, sizeof(anim_frame_t));
    anim->frames = frames_for_anim;
    anim->total_dur = 0.f;

    for (int i = 0; i < num_frames; ++i) {
        char texture_name[128] = {0};
        file_gets(texture_name, 128, fp);

        for (int j = 0; j < 128; ++j)
            if (texture_name[j] == '\n') {texture_name[j] = '\0'; break;}

        int x, y, w, h, offx, offy;

        file_gets(line, 1024, fp);
        sscanf(line, "%d %d %d %d %d %d", &x, &y, &w, &h, &offx, &offy);

        float dur = 1.f / 60.f;

        file_gets(line, 1024, fp);
        sscanf(line, "%f", &dur);

        tex_t *frame_asset = as_get_tex(texture_name);

        anim->frames[i].tex     = frame_asset;
        anim->frames[i].clip[0] = (float)x;
        anim->frames[i].clip[1] = (float)y;
        anim->frames[i].clip[2] = (float)w;
        anim->frames[i].clip[3] = (float)h;
        anim->frames[i].ox      = (float)offx;
        anim->frames[i].oy      = (float)offy;
        anim->frames[i].dur     = dur;

        anim->total_dur += dur;
    }

    out:
        file_close(fp);
        return ret;
}

static void _on_spritesheet_opt(void *ctx, const char *opt, const char *val)
{
    if (spritesheet_table_exists(&_spritesheets.table, opt))
        return;
    spritesheet_t tmp;
    if (_load_spritesheet(&tmp, val))
        return;
    spritesheet_t *ss = obj_pool_reserve(&_spritesheets.pool);
    *ss = tmp;
    spritesheet_table_insert(&_spritesheets.table, opt, ss);
}

static int _load_spritesheets()
{
    int num = 32;
    obj_pool_init(&_spritesheets.pool, num, sizeof(spritesheet_t));
    spritesheet_table_einit(&_spritesheets.table, num);
    return parse_cfg_file("spritesheets.cfg", _on_spritesheet_opt, 0);
}

static int _load_spritesheet(spritesheet_t *sheet, const char *fp)
{
    memset(sheet, 0, sizeof(spritesheet_t));

    file_t *f = file_open(fp, "r");
    if (!f) {return 1;}

    int ret = 0;

    char  line[512];
    char  str_buf[512];
    int   line_len  = 512;
    int   num_clips = 0;
    int   index     = 0;
    float tmpf;
    float clip[4];

    while(file_gets(line, line_len, f))
        if (sscanf(line, "%511s %f %f %f %f", str_buf, &tmpf, &tmpf, &tmpf,
            &tmpf) == 5)
            ++num_clips;

    sheet->clips        = calloc(num_clips, sizeof(float) * 4);
    sheet->clip_names   = calloc(num_clips, sizeof(char*));
    if(!sheet->clips || !sheet->clip_names)
        {ret = 2; goto out;}

    sheet->num_clips = num_clips;

    file_rewind(f);

    file_gets(line, line_len, f);

    if (strncmp(line, "PATH: ", 6))
        {ret = 3; goto out;}

    strncpy(str_buf, line + 6, 512);
    str_strip_trailing_spaces(str_buf);
    str_strip_non_ascii(str_buf);
    str_strip_ctrl_chars(str_buf);

    sheet->ta = as_get_tex(str_buf);
    if (!sheet->ta)
        {ret = 4; goto out;}

    while (file_gets(line, line_len, f))
    {
        if (sscanf(line, "%511s %f %f %f %f", str_buf, &clip[0], &clip[1],
            &clip[2], &clip[3]) == 5) {
            sheet->clips[index][0]      = clip[0];
            sheet->clips[index][1]      = clip[1];
            sheet->clips[index][2]      = clip[2];
            sheet->clips[index][3]      = clip[3];
            sheet->clip_names[index]    = create_dynamic_str(str_buf);
            DEBUG_PRINTFF("created clip '%s'\n", str_buf);
            if (!sheet->clip_names[index])
                {ret = 5; goto out;}
            index++;
        } else
            DEBUG_PRINTFF("failed to scan clip %d.\n", index);
    }

    out:
        if (ret) {
            for (int i = 0; i < sheet->num_clips; ++i)
                free_dynamic_str(sheet->clip_names[i]);
            free(sheet->clip_names);
            free(sheet->clips);
            DEBUG_PRINTFF("failed to load '%s' with code %d!\n", fp, ret);
        } else
            DEBUG_PRINTFF("loaded '%s' successfully\n", fp);
        file_close(f);
        return ret;
}
