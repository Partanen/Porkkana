#ifndef __ANDROID__
#include <SDL2/SDL.h>
#else
#include <SDL.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <assert.h>
#include "core.h"
#include "render.h"
#include "gui.h"
#include "util.h"
#include "assets.h"

#define BUTTON_BIT(btn) SDL_BUTTON(btn)
#define BUTTON_LEFT     SDL_BUTTON_LEFT
#define BUTTON_RIGHT    SDL_BUTTON_RIGHT
#define BUTTON_MIDDLE   SDL_BUTTON_MIDDLE

int                         main_window_w;
int                         main_window_h;
static gui_button_style_t  _button_style1;
gui_button_style_t          *button_style1 = &_button_style1;
static gui_win_style_t     _window_style1;
gui_win_style_t             *window_style1 = &_window_style1;
config_t                    config;
sfont_t                     *default_font;
sfont_t                     *small_font;

static int                  _running;
static SDL_Window           *_sdl_window;
static screen_t             *_active_screen, *_next_active_screen;
#ifdef __ANDROID__
static int                  _finger_pressed;
static SDL_FingerID         _finger_id;
#endif

static struct {
    uint32  buttons;
    uint32  last_buttons;
    int     x, y, last_x, last_y, down_x, down_y;
} mouse_state;

static struct {
    const uint8 *keys;
    uint8       *last_keys;
    int         num;
} keyboard_state;

struct
{
    double      delta;      /* Seconds */
    uint64      last_tick;
    uint32      fps;
    uint32      target_fps;
    uint64      frame_num;
} _clock;

static void _handle_events();
static void _update();
static void _update_gui_inputs(gui_input_state_t *is);
static double _tick_clock();
static void _on_config_opt(void *ctx, const char *opt, const char *val);
static int _read_config();
static int _write_config();
static void _handle_window_event(SDL_Event *e);

int core_init()
{
    DEBUG_PRINTFF("Reading config\n");
    _read_config();
    int ret = 0;
    int r;
    if (SDL_Init(SDL_INIT_VIDEO)) {
        ret = 1;
        goto out;
    }
    DEBUG_PRINTFF("Creating window\n");
    uint32 window_flags = SDL_WINDOW_OPENGL|SDL_WINDOW_RESIZABLE;
    window_flags |= (config.maximize_window ? SDL_WINDOW_MAXIMIZED : 0);
    if (!(_sdl_window = SDL_CreateWindow("Porkkana",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, config.window_size[0],
        config.window_size[1], window_flags))) {
        ret = 2;
        goto out;
    }
    DEBUG_PRINTFF("Initializing renderer\n");
    if ((r = r_init(_sdl_window))) {
        DEBUG_PRINTFF("r_init() returned %d\n", r);
        ret = 3;
        goto out;
    }
    DEBUG_PRINTFF("Loading assets\n");
    if (as_init()) {
        ret = 4;
        goto out;
    }
    DEBUG_PRINTFF("Initializing gui\n");
    if (gui_init(64, _update_gui_inputs, r_render_gui)) {
        ret = 5;
        goto out;
    }
    sfont_t *munro20 = as_get_font("munro20");
    sfont_t *munro10 = as_get_font("munro10");
    _button_style1                          = gui_create_button_style();
    _button_style1.normal.font              = munro20;
    _button_style1.hovered.font             = munro20;
    _button_style1.pressed.font             = munro20;
    _button_style1.normal.br_col[0]         = 0;
    _button_style1.normal.br_col[1]         = 0;
    _button_style1.normal.br_col[2]         = 0;
    _button_style1.normal.br_col[3]         = 255;
    _button_style1.normal.title_scale[0]    = 1.f;
    _button_style1.normal.title_scale[1]    = 1.f;
    _button_style1.hovered.br_col[0]        = 0;
    _button_style1.hovered.br_col[1]        = 0;
    _button_style1.hovered.br_col[2]        = 0;
    _button_style1.hovered.br_col[3]        = 255;
    _button_style1.hovered.title_scale[0]   = 1.f;
    _button_style1.hovered.title_scale[1]   = 1.f;
    _button_style1.pressed.br_col[0]        = 0;
    _button_style1.pressed.br_col[1]        = 0;
    _button_style1.pressed.br_col[2]        = 0;
    _button_style1.pressed.br_col[3]        = 255;
    _button_style1.pressed.title_scale[0]   = 1.f;
    _button_style1.pressed.title_scale[1]   = 1.f;
    _button_style1.normal.bw_t              = 2;
    _button_style1.normal.bw_b              = 2;
    _button_style1.normal.bw_l              = 2;
    _button_style1.normal.bw_r              = 2;
    _button_style1.hovered.bw_t             = 2;
    _button_style1.hovered.bw_b             = 2;
    _button_style1.hovered.bw_l             = 2;
    _button_style1.hovered.bw_r             = 2;
    _button_style1.pressed.bw_t             = 2;
    _button_style1.pressed.bw_b             = 2;
    _button_style1.pressed.bw_l             = 2;
    _button_style1.pressed.bw_r             = 2;
    _button_style1.normal.title_offset[1]   = -5;
    _button_style1.hovered.title_offset[1]  = -5;
    _button_style1.pressed.title_offset[1]  = -5;

    _window_style1 = gui_create_win_style();
    uint8 color[4] = {0, 0, 0, 255};
    memcpy(_window_style1.active.br_col, color, sizeof(color));
    memcpy(_window_style1.hovered.br_col, color, sizeof(color));
    memcpy(_window_style1.inactive.br_col, color, sizeof(color));
    memcpy(_window_style1.hovered.bg_col, _window_style1.inactive.bg_col,
        sizeof(_window_style1.inactive.bg_col));
    memcpy(_window_style1.active.bg_col, _window_style1.inactive.bg_col,
        sizeof(_window_style1.inactive.bg_col));
    _window_style1.active.bw_t      = 2;
    _window_style1.active.bw_b      = 2;
    _window_style1.active.bw_l      = 2;
    _window_style1.active.bw_r      = 2;
    _window_style1.hovered.bw_t     = 2;
    _window_style1.hovered.bw_b     = 2;
    _window_style1.hovered.bw_l     = 2;
    _window_style1.hovered.bw_r     = 2;
    _window_style1.inactive.bw_t    = 2;
    _window_style1.inactive.bw_b    = 2;
    _window_style1.inactive.bw_l    = 2;
    _window_style1.inactive.bw_r    = 2;

    gui_font(munro20);
    default_font    = munro20;
    small_font      = munro10;

    /*-- Clock --*/
    _clock.delta        = 0;
    _clock.last_tick    = 0;
    _clock.fps          = 0;
    _clock.frame_num    = 0;
    _clock.target_fps   = 60;

    srand((int)time(0));

    keyboard_state.keys         = SDL_GetKeyboardState(&keyboard_state.num);
    keyboard_state.last_keys    = calloc(keyboard_state.num, 1);

    out:
        if (ret)
            DEBUG_PRINTFF("error %d\n", ret);
        else
            _write_config();
        return ret;
}

int core_run()
{
    _running = 1;
    while (_running) {
        _handle_events();
        _update();
    }
    return 0;
}

void core_shutdown()
    {_running = 0;}

float core_compute_target_viewport_scale()
{
    float sx = (float)main_window_w / (float)RESOLUTION_W;
    float sy = (float)main_window_h / (float)RESOLUTION_H;
    float s = sx > sy ? sy : sx;
    return s;
}

float core_compute_target_viewport(int *ret)
{
    float s = core_compute_target_viewport_scale();
    ret[2] = (int)(s * (float)(RESOLUTION_W));
    ret[3] = (int)(s * (float)(RESOLUTION_H));
    ret[0] = main_window_w / 2 - ret[2] / 2;
    ret[1] = main_window_h / 2 - ret[3] / 2;
    return s;
}

void core_set_screen(screen_t *screen)
    {_next_active_screen = screen;}

int core_is_key_down(int key)
{
    if (keyboard_state.num && key >= 0 && key < keyboard_state.num)
        return (int)keyboard_state.keys[key];
    return 0;
}

int core_key_down_now(int key)
{
    if (!keyboard_state.num || key < 0 || key >= keyboard_state.num)
        return 0;
    return keyboard_state.keys[key] && !keyboard_state.last_keys[key];
}

int core_key_up_now(int key)
{
    if (!keyboard_state.num || key < 0 || key >= keyboard_state.num)
        return 0;
    return !keyboard_state.keys[key] && keyboard_state.last_keys[key];
}

void panic(int err, const char *comment)
{
    printf("Panic (%d): \"%s\"\n", err, comment);
    exit(err);
}

unsigned int core_get_mouse_state(int *ret_x, int *ret_y)
{
    if (ret_x)
        *ret_x = mouse_state.x;
    if (ret_y)
        *ret_y = mouse_state.y;
    return mouse_state.buttons;
}

int core_button_down_now(unsigned int button)
{
    if (gui_is_any_element_pressed()) {
        DEBUG_PRINTFF("gui was clicked, returning 0\n");
        return 0;
    }
    unsigned int last   = mouse_state.last_buttons & button;
    unsigned int now    = mouse_state.buttons & button;
    return now && !last;
}

int core_button_up_now(unsigned int button)
{
    if (gui_is_any_element_pressed()) {
        DEBUG_PRINTFF("gui was released, returning 0\n");
        return 0;
    }
    unsigned int last   = mouse_state.last_buttons & button;
    unsigned int now    = mouse_state.buttons & button;
    return !now && last;
}

unsigned int core_get_fps()
    {return _clock.fps;}

static void _handle_events()
{
    mouse_state.last_x          = mouse_state.x;
    mouse_state.last_y          = mouse_state.y;
    mouse_state.last_buttons    = mouse_state.buttons;

    memcpy(keyboard_state.last_keys, keyboard_state.keys, keyboard_state.num);

    SDL_Event e;
    while (SDL_PollEvent(&e)) {
        switch (e.type) {
        case SDL_QUIT:
            _running = 0;
            break;
        case SDL_KEYDOWN:
            break;
        case SDL_WINDOWEVENT: {
            _handle_window_event(&e);
            break;
#ifdef __ANDROID__
        case SDL_FINGERDOWN:
            if (_finger_pressed)
                break;
            mouse_state.x       = e.tfinger.x * (float)main_window_w;
            mouse_state.y       = e.tfinger.y * (float)main_window_h;
            mouse_state.buttons = 1;
            _finger_id          = e.tfinger.touchId;
            break;
        case SDL_FINGERUP:
            if (e.tfinger.touchId == _finger_id)
                mouse_state.buttons = 0;
            break;
        case SDL_FINGERMOTION:
            if (e.tfinger.touchId != _finger_id)
                break;
            mouse_state.x = e.tfinger.x * (float)main_window_w;
            mouse_state.y = e.tfinger.y * (float)main_window_h;;
            break;
#endif
        }
        }
    }
    SDL_GetWindowSize(_sdl_window, &main_window_w, &main_window_h);

#ifndef __ANDROID__
    int mx, my;
    mouse_state.buttons = SDL_GetMouseState(&mx, &my);
    mouse_state.x       = mx;
    mouse_state.y       = my;
#endif
}

static void _update()
{
    double dt = _tick_clock();
    if (_next_active_screen != _active_screen) {
        gui_clear();
        if (_active_screen && _active_screen->close)
            _active_screen->close();
        _active_screen = _next_active_screen;
        if (_active_screen && _active_screen->open)
            _active_screen->open();
    }
    if (_active_screen && _active_screen->update)
        _active_screen->update(dt);
    r_swap_buffers(_sdl_window);
}

static void _update_gui_inputs(gui_input_state_t *is)
{
    int     vp[4];
    float   sc = core_compute_target_viewport(vp);
    sc = MAX(sc, 0.0000001f);

    uint32 s    = mouse_state.buttons;

    is->mx      = (int)((float)(mouse_state.x - vp[0]) / sc);
    is->my      = (int)((float)(mouse_state.y - vp[1]) / sc);

    is->buttons = 0;
    is->buttons |= ((s & BUTTON_BIT(BUTTON_LEFT))  ? GUI_MB_LEFT   : 0);
    is->buttons |= ((s & BUTTON_BIT(BUTTON_RIGHT)) ? GUI_MB_RIGHT  : 0);
    is->buttons |= ((s & BUTTON_BIT(BUTTON_MIDDLE))? GUI_MB_MIDDLE : 0);

    is->coord_space[0] = 0;
    is->coord_space[1] = 0;
    is->coord_space[2] = RESOLUTION_W;
    is->coord_space[3] = RESOLUTION_H;
}

static double _tick_clock()
{
    uint64 tick_time = SDL_GetPerformanceCounter();
    uint64 perf_freq = SDL_GetPerformanceFrequency();

    if (_clock.last_tick != 0.0f)
        _clock.delta = (double)(tick_time - _clock.last_tick) / (double)perf_freq;
    else
        _clock.delta = 0.016f;

    _clock.last_tick = tick_time;

    if (_clock.delta > 0.0f)
        _clock.fps = (uint32)(1.0f / _clock.delta);

    if (_clock.target_fps != 0) {
        double target_time = 1.0f / (double)_clock.target_fps * 1000.0f;

        if ((double)(tick_time - _clock.last_tick) < target_time) {
            uint64 substraction = tick_time - _clock.last_tick;
            SDL_Delay((uint)(target_time - substraction));
        }
    }
    _clock.frame_num++;
    return _clock.delta;
}

static void _on_config_opt(void *ctx, const char *opt, const char *val)
{
    if (streq(opt, "maximize_window"))
        str_to_bool(val, &config.maximize_window);
    else if (streq(opt, "window_size")) {
        int v[2];
        if (sscanf(val, "%dx%d", &v[0], &v[1]) != 2)
            return;
        if (v[0] <= 0 || v[1] <= 0)
            return;
        config.window_size[0] = v[0];
        config.window_size[1] = v[1];
    }
}

static int _read_config()
{
    config.maximize_window  = 0;
    config.window_size[0]   = RESOLUTION_W;
    config.window_size[1]   = RESOLUTION_H;
    return parse_cfg_file("config.cfg", _on_config_opt, 0);
}

static int _write_config()
{
    FILE *f = fopen("config.cfg", "w+");
    if (!f)
        return 1;
    const char *st = "true";
    const char *sf = "false";
    int ww, wh;
    SDL_GetWindowSize(_sdl_window, &ww, &wh);
    fprintf(f, "maximize_window = %s\n", config.maximize_window ? st : sf);
    fprintf(f, "window_size     = %dx%d", ww >= 0 ? ww : RESOLUTION_W,
        wh >= 0 ? wh : RESOLUTION_H);
    return 0;
}

static void _handle_window_event(SDL_Event *e)
{
    switch (e->window.type) {
    case SDL_WINDOWEVENT_MINIMIZED:
        DEBUG_PRINTF("SDL_WINDOWEVENT_MINIMIZED\n");
        break;
    case SDL_WINDOWEVENT_ENTER:
        DEBUG_PRINTFF("SDL_WINDOWEVENT_ENTER\n");
        break;
    case SDL_WINDOWEVENT_FOCUS_LOST:
        DEBUG_PRINTFF("SDL_WINDOWEVENT_FOCUS_LOST\n");
        break;
    }
}
