#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>
#include <ctype.h>
#include "types.h"

typedef struct darr_head_t      darr_head_t;
typedef struct obj_pool_alloc_t obj_pool_alloc_t;
typedef struct obj_pool_alloc_t obj_pool_alloc_darr_t;
typedef struct obj_pool_t       obj_pool_t;

struct darr_head_t {
    uint32 num;
    uint32 cap;
};

struct obj_pool_alloc_t {
    void    *mem;
    uint32  num;
};

struct obj_pool_t {
    uint32                  item_size;
    obj_pool_alloc_darr_t   *allocs;
    void_ptr_darr_t         *free;
    uint32                  cap;
};

#ifdef __ANDROID__
#include <SDL.h>
#endif

uint8 *load_file_to_buffer(const char *path, size_t *len);
dchar *load_text_file_to_dstr(const char *path);
dchar *create_dynamic_str(const char *txt);
void free_dynamic_str(const dchar *str);
dchar *create_empty_dynamic_str(uint len);
dchar *set_dynamic_str_len(dchar *str, uint len);
dchar *set_dynamic_str(dchar *str, const char *txt);

void
dstr_set(dchar **dstr, const char *s);

#define FNV_32_SEED     ((uint32)0x811C9DC5)
#define FNV_32_PRIME    ((uint32)0x01000193)
#define FNV_64_SEED     ((uint64)0xcbf29ce484222325)
#define FNV_64_PRIME    ((uint64)1099511628211)

#ifndef __ANDROID__
#define DEBUG_PRINTF(str, ...) printf((str), ##__VA_ARGS__)
#define DEBUG_PRINTFF(str, ...) printf("%s: " str, __func__, ##__VA_ARGS__)
#else
#include <android/log.h>
#define DEBUG_PRINTF(str, ...) __android_log_print(ANDROID_LOG_INFO, \
    "Porkkana", str, ##__VA_ARGS__)
#define DEBUG_PRINTFF(str, ...) __android_log_print(ANDROID_LOG_INFO, \
    "Porkkana", "%s: " str, __func__, ##__VA_ARGS__)
#endif

#ifndef __ANDROID__
typedef FILE file_t;
#define file_open(fp_, args_) fopen((fp_), (args_))
#define file_tell(f_) ftell((f_))
#define file_getc(f_) fgetc((f_))
#define file_close(f_) fclose((f_))
#define file_seek(f_, p_, o_) fseek((f_), (p_), (o_))
#define file_gets(b_, l_, f_) fgets((b_), (l_), (f_))
#define file_rewind(f_) rewind(f_)
enum file_seek {
    FILE_SEEK_SET = SEEK_SET,
    FILE_SEEK_CUR = SEEK_CUR,
    FILE_SEEK_END = SEEK_END,
};
#else
typedef SDL_RWops file_t;
#define file_open(fp_, args_) SDL_RWFromFile((fp_), (args_))
#define file_tell(f_) SDL_RWtell((f_))
static inline int file_getc(file_t *f)
{
    if (!f->size(f) || file_tell(f) >= f->size(f) - 1)
        return EOF;
    char    c;
    return f->read(f, &c, 1, 1) ? (int)c : EOF;
}
#define file_close(f_) SDL_RWclose((f_))
#define file_seek(f_, p_, o_) (f_)->seek((f_), (p_), (o_))
static inline char *file_gets(char *b, int l, file_t *f)
{
    if (l <= 0)
        return 0;
    int num = 0;
    int d, i = 0;
    int eof = 0;
    for (;;) {
        d = file_getc(f);
        if (d == EOF) {
            eof = 1;
            break;
        }
        b[i++] = (char)d;
        if (!d || d == '\n')
            break;
        if (i == l)
            break;
    }
    b[i < l ? i : i - 1] = 0;
    return eof ? 0 : b;
}
enum file_seek {
    FILE_SEEK_SET = RW_SEEK_SET,
    FILE_SEEK_CUR = RW_SEEK_CUR,
    FILE_SEEK_END = RW_SEEK_END,
};
#define file_rewind(f_) file_seek((f_), 0, FILE_SEEK_SET)
#endif

#define PI 3.14159265358979323846
#define MIN(val, min) ((val) < (min) ? (val) : (min))
#define MAX(val, max) ((val) > (max) ? (val) : (max))
#define CLAMP(val, min, max) (MAX((MIN((val), (max))), (min)))
#define ABS(val) ((val) < 0 ? -(val) : (val))
#define FLOORF(val) (val >= 0.0f ? (float)(int)(val) : (float)((int)(val) - 1))
#define CEILF(val) ((val) - (float)(int)(val) != 0.0f ? \
    (val) > 0.0f ? ((float)((int)(val) + 1)) : (float)(int)(val) : \
    val)
#define ROUNDF(val) ((val) >= 0.0f ? \
    ((val) - (float)(int)(val) >= 0.5f ? \
        (float)((int)(val) + 1) : (float)(int)(val)) : \
    ((val) + (float)(-(int)(val)) <= -0.5f ? \
         (float)((int)(val) - 1) : (float)(int)(val)))

#define GET_BITFLAG(val, flag)      (((val) & (flag)) == (flag))
#define SET_BITFLAG_ON(val, flag)   ((val) |= (flag))
#define SET_BITFLAG_OFF(val, flag)  ((val) &= ~(flag))
#define NEXT_POW2(v) \
    ((((v) - 1) | (v >> 1) | (v >> 2) | (v >> 4) | (v >> 8) | v >> 16) + 1)

void *darr_ensure_growth_by(void *darr, uint32 grow_by, uint32 item_sz);
void darr_sized_erase(void *darr, uint32 index, uint32 item_sz);

#define _darr_head(darr) ((darr_head_t*)(darr) - 1)
#define darr_head(darr) ((darr) ? _darr_head(darr) : 0)
#define darr_num(darr)  ((darr) ? ((darr_head_t*)(darr) - 1)->num : 0)
#define darr_cap(darr)  ((darr) ? ((darr_head_t*)(darr) - 1)->cap : 0)
#define darr_reserve(darr, cap_) \
    ((darr) = darr_ensure_growth_by((darr), \
        darr_num(darr) < (cap_) ? (cap_) - darr_num(darr) : 0, \
        sizeof(*(darr))))
#define darr_free(darr) (free(darr_head(darr)), (darr) = 0)
#define darr_push(darr, val) \
    ((darr) = darr_ensure_growth_by((darr), 1, sizeof(*(darr))), \
    (darr)[darr_head(darr)->num++] = (val))
#define darr_push_empty(darr) \
    ((darr) = darr_ensure_growth_by((darr), 1, sizeof(*(darr))), \
    &(darr)[darr_head(darr)->num++])
#define darr_clear(darr) \
    ((darr) ? _darr_head(darr)->num = 0 : 0)
#define darr_erase_last(darr) \
    ((darr) ? (_darr_head(darr)->num ? _darr_head(darr)->num-- : 0) : 0)
#define darr_erase(darr, index) \
    (darr_sized_erase(darr, index, sizeof(*(darr))))

static inline uint32 fnv_hash32_from_str(const char *str);
static inline uint32 fnv_hash32_from_data(const void *str, size_t len);
static inline uint64 fnv_hash64_from_str(const char *str);
static inline uint64 fnv_hash64_from_data(const void *str, size_t len);
static inline bool32 streq(const char *sa, const char *sb);
void str_strip_trailing_spaces(char *str);
bool32 str_is_ascii(const char *str);
int str_strip_ctrl_chars(char *str);
int str_strip_non_ascii(char *str);
void str_to_upper(char *str);
void str_to_lower(char *str);
int str_to_bool(const char *str, bool32 *ret_bool);
int parse_cfg_file(const char *path,
    void (*callback)(void *ctx, const char *opt, const char *val),
    void *context);
int parse_def_file(const char *fp,
    int (*on_def)(void *ctx, const char *def, const char *val),
    int (*on_opt)(void *ctx, const char *opt, const char *val),
    void *ctx);

void obj_pool_init(obj_pool_t *op, uint32 num_items, uint32 item_size);
void obj_pool_destroy(obj_pool_t *op);
void obj_pool_clear(obj_pool_t *op);
void *obj_pool_reserve(obj_pool_t *op);
void *obj_pool_reserve_static(obj_pool_t *op);
void obj_pool_free(obj_pool_t *op, void *ptr);

#define DYNAMIC_HASH_TABLE_DEFINITION(table_name, type, hash_input_type, \
    hash_type, hash_func, bucket_sz) \
\
typedef struct table_name##_item_t      table_name##_item_t; \
typedef struct table_name##_bucket_t    table_name##_bucket_t; \
typedef struct table_name##_t           table_name##_t; \
\
struct table_name##_item_t \
{ \
    type        val; \
    hash_type   hash; \
}; \
\
struct table_name##_bucket_t \
{ \
    table_name##_item_t items[bucket_sz]; \
    uint8               res_flags; \
}; \
\
struct table_name##_t \
{ \
    table_name##_bucket_t   *buckets; \
    size_t                  num_buckets; \
    size_t                  num_items; \
}; \
\
static inline int \
table_name##_init(table_name##_t *t, size_t sz); \
\
static inline void \
table_name##_einit(table_name##_t *t, size_t sz); \
\
static inline void \
table_name##_destroy(table_name##_t *t); \
\
static inline int \
table_name##_resize(table_name##_t *t, size_t new_num_buckets); \
/* A return value of < 0 indicates out of memory. > 0 indicates having run \
 * out of slots while rearranging the internal containers. */ \
\
static inline type * \
table_name##_insert_empty_by_hash(table_name##_t *t, hash_type hash); \
\
static inline type * \
table_name##_einsert_empty_by_hash(table_name##_t *t, hash_type hash); \
\
static inline int \
table_name##_insert_by_hash(table_name##_t *t, hash_type hash, type val); \
\
static inline void \
table_name##_einsert_by_hash(table_name##_t *t, hash_type hash, type val); \
\
static inline type * \
table_name##_insert_empty(table_name##_t *t, hash_input_type hi); \
\
static inline type * \
table_name##_einsert_empty(table_name##_t *t, hash_input_type hi); \
\
static inline int \
table_name##_insert(table_name##_t *t, hash_input_type hi, type val); \
\
static inline void \
table_name##_einsert(table_name##_t *t, hash_input_type hi, type val); \
\
static inline void \
table_name##_erase(table_name##_t *t, hash_input_type hi); \
\
static inline type * \
table_name##_get_ptr_by_hash(table_name##_t *t, hash_type hash); \
\
static inline type * \
table_name##_get_ptr(table_name##_t *t, hash_input_type hi); \
\
static inline type \
table_name##_get_by_hash(table_name##_t *t, hash_type hash); \
\
static inline type \
table_name##_get(table_name##_t *t, hash_input_type hi); \
\
static inline type \
table_name##_pop_by_hash(table_name##_t *t, hash_type h); \
\
static inline type \
table_name##_pop(table_name##_t *t, hash_input_type hi); \
\
static inline int \
table_name##_try_pop_by_hash(table_name##_t *t, hash_type hash, type *ret); \
\
static inline int \
table_name##_try_pop(table_name##_t *t, hash_input_type hi, type *ret); \
\
static inline int \
table_name##_exists(table_name##_t *t, hash_input_type hi); \
\
static inline void \
table_name##_clear(table_name##_t *t); \
\
static inline size_t \
table_name##_bucket_sz(); \
\
static inline type * \
table_name##_get_from_bucket_at(table_name##_t *t, size_t bucket_index, \
    size_t item_index); \
\
static inline int \
table_name##_init(table_name##_t *t, size_t num_buckets) \
{ \
    table_name##_t tmp = {0}; \
    tmp.buckets = calloc(num_buckets, sizeof(table_name##_bucket_t)); \
    if (!tmp.buckets) return 1; \
    tmp.num_buckets = num_buckets; \
    tmp.num_items   = 0; \
    *t = tmp; \
    return 0; \
} \
\
static inline void \
table_name##_einit(table_name##_t *t, size_t num_buckets) \
    {if (table_name##_init(t, num_buckets)) exit(EXIT_FAILURE);} \
\
static inline void \
table_name##_destroy(table_name##_t *t) \
{ \
    free(t->buckets); \
    memset(t, 0, sizeof(table_name##_t)); \
} \
\
static inline int \
table_name##_resize(table_name##_t *t, size_t new_num_buckets) \
{ \
    table_name##_t nt; \
    if (table_name##_init(&nt, new_num_buckets)) \
        return 1; \
\
    size_t  old_max = t->num_buckets; \
    int     j; \
\
    for (size_t i = 0; i < old_max; ++i) \
    { \
        for (j = 0; j < bucket_sz; ++j) \
        { \
            if (!(t->buckets[i].res_flags & (1 << j))) \
                continue; \
            if (table_name##_insert_by_hash(&nt, t->buckets[i].items[j].hash, \
                t->buckets[i].items[j].val)) \
                {table_name##_destroy(&nt); return 2;} \
        } \
    } \
\
    free(t->buckets); \
    *t = nt; \
    return 0; \
} \
\
static inline type * \
table_name##_insert_empty_by_hash(table_name##_t *t, hash_type hash) \
{ \
    if (!t->num_buckets && table_name##_init(t, 8)) \
        return 0; \
    size_t max_bs   = t->num_buckets; \
    size_t index    = hash % max_bs; \
\
    table_name##_bucket_t   *b = &t->buckets[index]; \
\
    size_t max_items    = max_bs * bucket_sz; \
    size_t prcnt        = 100 * t->num_items / max_items; \
\
    /* If more than 70% of the table is filled, enlarge. */ \
    if (prcnt >= 70) \
    { \
        size_t req_max_bs = max_bs + 16; \
        size_t new_max_bs = max_bs * 2; \
        if (new_max_bs <= req_max_bs) \
            new_max_bs = req_max_bs; \
        if (!table_name##_resize(t, new_max_bs)) \
        { \
            max_bs  = t->num_buckets; \
            index   = hash % max_bs; \
            b       = &t->buckets[index]; \
        } \
    } \
\
    type *slot = 0; \
\
    for (;;) \
    { \
        for (int i = 0; i < bucket_sz; ++i) \
            if (b->res_flags & (1 << i) && b->items[i].hash == hash) \
                exit(EXIT_FAILURE); \
        for (int i = 0; i < bucket_sz; ++i) \
        { \
            table_name##_item_t *item = &b->items[i]; \
            if (b->res_flags & (1 << i)) \
                continue; \
            slot            = &item->val; \
            item->hash      = hash; \
            b->res_flags |= (1 << i); \
            t->num_items++; \
            break; \
        } \
        if (slot) break; \
        if (table_name##_resize(t, t->num_buckets * 2)) \
            return 0; \
        index   = hash % t->num_buckets; \
        b       = &t->buckets[index]; \
    } \
\
    return slot; \
} \
\
static inline type * \
table_name##_einsert_empty_by_hash(table_name##_t *t, hash_type hash) \
{ \
    type *ret = table_name##_insert_empty_by_hash(t, hash); \
    if (!ret) \
        exit(EXIT_FAILURE); \
    return ret; \
} \
\
static inline int \
table_name##_insert_by_hash(table_name##_t *t, hash_type hash, type val) \
{ \
    type *sv = table_name##_insert_empty_by_hash(t, hash); \
    if (sv) {*sv = val; return 0;} \
    return 1; \
} \
\
static inline void \
table_name##_einsert_by_hash(table_name##_t *t, hash_type hash, type val) \
{ \
    if (table_name##_insert_by_hash(t, hash, val)) \
        exit(EXIT_FAILURE); \
} \
\
static inline type * \
table_name##_insert_empty(table_name##_t *t, hash_input_type hi) \
{ \
    hash_type hash = hash_func(hi); \
    return table_name##_insert_empty_by_hash(t, hash); \
} \
static inline type * \
table_name##_einsert_empty(table_name##_t *t, hash_input_type hi) \
{ \
    type *ret = table_name##_insert_empty(t, hi); \
    if (!ret) exit(EXIT_FAILURE); \
    return ret; \
} \
\
static inline int \
table_name##_insert(table_name##_t *t, hash_input_type hi, type val) \
{ \
    type *sv = table_name##_insert_empty(t, hi); \
    if (sv) {*sv = val; return 0;} \
    return 1; \
} \
\
static inline void \
table_name##_einsert(table_name##_t *t, hash_input_type hi, type val) \
{ \
    int r = table_name##_insert(t, hi, val); \
    if (r) exit(EXIT_FAILURE); \
} \
\
static inline void \
table_name##_erase_by_hash(table_name##_t *t, hash_type hash) \
{ \
    size_t index = hash % t->num_buckets; \
    table_name##_bucket_t *b = &t->buckets[index]; \
    uint8 res_flags = b->res_flags; \
    for (int i = 0; i < bucket_sz; ++i) \
    { \
        if (res_flags & (1 << i) && b->items[i].hash == hash) \
            {b->res_flags &= ~(1 << i); t->num_items--; return;} \
    } \
} \
\
static inline void \
table_name##_erase(table_name##_t *t, hash_input_type hi) \
{ \
    hash_type hash = hash_func(hi); \
    table_name##_erase_by_hash(t, hash); \
} \
\
static inline type * \
table_name##_get_ptr_by_hash(table_name##_t *t, hash_type hash) \
{ \
    if (!t->num_buckets) return 0; \
    size_t index = hash % t->num_buckets; \
    table_name##_bucket_t *b = &t->buckets[index]; \
    uint8 res_flags = b->res_flags; \
    for (int i = 0; i < bucket_sz; ++i) \
        if (res_flags & (1 << i) && b->items[i].hash == hash) \
            return &b->items[i].val; \
    return 0; \
} \
static inline type * \
table_name##_get_ptr(table_name##_t *t, hash_input_type hi) \
{ \
    hash_type hash = hash_func(hi); \
    return table_name##_get_ptr_by_hash(t, hash); \
} \
\
static inline type \
table_name##_get_by_hash(table_name##_t *t, hash_type hash) \
{ \
    type ret; \
    type *ptr = table_name##_get_ptr_by_hash(t, hash); \
    if (ptr) \
        ret = *ptr; \
    else \
        memset(&ret, 0, sizeof(ret)); \
    return ret; \
} \
\
static inline type \
table_name##_get(table_name##_t *t, hash_input_type hi) \
{ \
    hash_type hash = hash_func(hi); \
    return table_name##_get_by_hash(t, hash); \
} \
static inline type \
table_name##_pop_by_hash(table_name##_t *t, hash_type hash) \
{ \
    type ret; \
    size_t index = hash % t->num_buckets; \
    table_name##_bucket_t *b = &t->buckets[index]; \
    uint8   res_flags   = b->res_flags; \
    bool32  found       = 0; \
    for (int i = 0; i < bucket_sz; ++i) \
        if (res_flags & (1 << i) && b->items[i].hash == hash) \
        { \
            ret = b->items[i].val; \
            res_flags &= ~(1 << i); \
            b->res_flags = res_flags; \
            t->num_items--; \
            found = 1; \
            break; \
        } \
    if (!found) \
        memset(&ret, 0, sizeof(ret)); \
    return ret; \
} \
\
static inline type \
table_name##_pop(table_name##_t *t, hash_input_type hi) \
{ \
    hash_type hash = hash_func(hi); \
    return table_name##_pop_by_hash(t, hash); \
} \
\
static inline int \
table_name##_try_pop_by_hash(table_name##_t *t, hash_type hash, type *ret) \
{ \
    size_t index = hash % t->num_buckets; \
    table_name##_bucket_t *b = &t->buckets[index]; \
    uint8 res_flags = b->res_flags; \
    for (int i = 0; i < bucket_sz; ++i) \
        if (res_flags & (1 << i) && b->items[i].hash == hash) \
        { \
            *ret = b->items[i].val; \
            res_flags &= ~(1 << i); \
            b->res_flags = res_flags; \
            t->num_items--; \
            return 0; \
        } \
    return 1; \
} \
\
static inline int \
table_name##_try_pop(table_name##_t *t, hash_input_type hi, type *ret) \
{ \
    hash_type hash = hash_func(hi); \
    return table_name##_try_pop_by_hash(t, hash, ret); \
} \
\
static inline int \
table_name##_exists(table_name##_t *t, hash_input_type hi) \
    {return table_name##_get_ptr(t, hi) ? 1 : 0;} \
\
static inline void \
table_name##_clear(table_name##_t *t) \
{ \
    memset(t->buckets, 0, t->num_buckets * sizeof(table_name##_bucket_t)); \
    t->num_items = 0; \
} \
static inline size_t \
table_name##_bucket_sz() \
    {return bucket_sz;} \
\
static inline type * \
table_name##_get_from_bucket_at(table_name##_t *t, size_t bucket_index, \
    size_t item_index) \
{ \
    if (bucket_index >= t->num_buckets) return 0; \
    if (item_index >= bucket_sz)        return 0; \
    table_name##_bucket_t *b = &t->buckets[bucket_index]; \
    return b->res_flags & bucket_index ? &b->items[item_index].val : 0; \
}

static inline uint32 fnv_hash32_from_str(const char *str)
{
    uint32 hash = FNV_32_SEED;
    for (const char *c = str; *c; ++c)
        {hash ^= *c; hash *= FNV_32_PRIME;}
    return hash;
}

static inline uint32 fnv_hash32_from_data(const void *str, size_t len)
{
    uint32 hash = FNV_32_SEED;
    for (const char *c = str; c < (const char*)str + len; ++c)
        {hash ^= *c; hash *= FNV_32_PRIME;}
    return hash;
}

static inline uint64 fnv_hash64_from_str(const char *str)
{
    uint64 hash = FNV_64_SEED;
    for (const char *c = str; *c; ++c)
        {hash ^= *c; hash *= FNV_64_PRIME;}
    return hash;
}

static inline uint64 fnv_hash64_from_data(const void *str, size_t len)
{
    uint64 hash = FNV_64_SEED;
    for (const char *c = str; c < (const char*)str + len; ++c)
        {hash ^= *c; hash *= FNV_64_PRIME;}
    return hash;
}

static inline bool32 streq(const char *sa, const char *sb)
{
    uint32 i = 0;
    for (;; ++i) {
        if (sa[i] != sb[i])
            return 0;
        if (sa[i] == 0)
            return 1;
    }
}

#endif /* UTIL_H */
