#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "util.h"

#define STRIP_STR(str, call) \
    char *ca = str; \
    char *cb = str; \
    for (;*cb;) {*ca = *(cb++); if (call(*ca)) ca++;} \
    *ca = 0; \
    return (int)(ca - str);

#define NOT_CTRL(c_) (!iscntrl(c_))

typedef struct
{
    uint32 len, cap; /* Neither number includes the null terminator */
} dstr_header_t;

static dchar *_create_empty_dynamic_str_inner(uint len);
static dstr_header_t *_grow_dynamic_str_cap(dstr_header_t *h, uint cap);

#ifndef __ANDROID__
uint8 *load_file_to_buffer(const char *path, size_t *len)
{
    if (!path)
        return 0;

    uint8       *buf;
    FILE        *file;
    long int    num_chars;

    file = fopen(path, "rb");
    if (!file) return 0;

    /* Get file length */
    fseek(file, 0L, SEEK_END);
    num_chars = ftell(file);
    rewind(file);

    buf = (uint8*)malloc(num_chars);
    fread(buf, sizeof(char), num_chars, file);
    fclose(file);

    if (len)
        *len = num_chars;

    return buf;
}
#else
uint8 *load_file_to_buffer(const char *path, size_t *len)
{
    if (!path)
        return 0;

    uint8       *buf;
    SDL_RWops   *file;
    long int    num_chars;

    file = SDL_RWFromFile(path, "rb");
    if (!file) return 0;

    /* Get file length */
    num_chars = file->size(file);

    buf = (uint8*)malloc(num_chars);
    file->read(file, buf, sizeof(char), num_chars);
    SDL_FreeRW(file);

    if (len)
        *len = num_chars;

    return buf;
}
#endif

dchar *load_text_file_to_dstr(const char *path)
{
    FILE *f = fopen(path, "r");
    if (!f) return 0;

    char *str = 0;

    size_t  len = 0;
    int     c;
    for (c = fgetc(f); c != EOF; c = fgetc(f))
        len++;
    if (!len)
        goto out;

    rewind(f);

    str = create_empty_dynamic_str(len);
    if (!str)
        goto out;

    char *rc = str;
    while ((*(rc++) = fgetc(f)) != EOF);
    *rc = 0;
    set_dynamic_str_len(str, len);

    out:
        fclose(f);
        return str;
}

dchar *create_dynamic_str(const char *txt)
{
    if (!txt) return 0;
    uint32  len = strlen(txt);
    dchar *ret  = _create_empty_dynamic_str_inner(len);
    if (ret) {memcpy(ret, txt, len + 1); ((dstr_header_t*)ret - 1)->len = len;}
    return ret;
}

void free_dynamic_str(const dchar *str)
    {if (str) free((dstr_header_t*)str - 1);}

dchar *create_empty_dynamic_str(uint len)
{
    if (!len)
        return 0;
    return _create_empty_dynamic_str_inner(len);
}

dchar *set_dynamic_str_len(dchar *str, uint len)
{
    if (!str)
        return 0;

    dstr_header_t *h = (dstr_header_t*)str - 1;
    if (len > h->cap) {
        dstr_header_t *nh = _grow_dynamic_str_cap(h, len);
        if (!nh)
            return 0;
        h = nh;
    }
    h->len = len;
    ((dchar*)(h + 1))[len] = 0;
    return (dchar*)(h + 1);
}

dchar *set_dynamic_str(dchar *str, const char *txt)
{
    if (!str) return create_dynamic_str(txt);
    if (!txt) return 0;
    uint32 len = strlen(txt);
    if (!len) return 0;

    dstr_header_t *h = (dstr_header_t*)str - 1;

    if (h->cap < len) {
        dstr_header_t *nh = _grow_dynamic_str_cap(h, len);
        if (!nh) {return 0;}
        h = nh;
    }

    h->len = len;
    char *ret = (char*)(h + 1);
    memcpy(ret, txt, len + 1);
    return ret;
}

void dstr_set(dchar **dstr, const char *s)
{
    if (!dstr)
        return;
    dchar *ret = set_dynamic_str(*dstr, s);
    if (!ret && s && s[0])
        exit(EXIT_FAILURE);
    *dstr = ret;
}

void *emalloc(size_t sz)
{
    void *ret = malloc(sz);
    if (!ret && sz)
        exit(EXIT_FAILURE);
    return ret;
}

void *erealloc(void *p, size_t sz)
{
    void *ret = realloc(p, sz);
    if (!ret && sz)
        exit(EXIT_FAILURE);
    return ret;
}

void *darr_ensure_growth_by(void *darr, uint32 grow_by, uint32 item_sz)
{
    if (!grow_by)
        return darr;
    darr_head_t *h = darr_head(darr);
    if (!h) {
        uint32 cap = grow_by > 4 ? grow_by : 4;
        h = emalloc(sizeof(darr_head_t) + cap * item_sz);
        h->num = 0;
        h->cap = cap;
    } else {
        uint32 req = h->num + grow_by;
        if (req <= h->cap)
            return darr;
        uint32 new_cap = h->cap * 2;
        new_cap = new_cap > req ? new_cap : req;
        h = erealloc(h, sizeof(darr_head_t) + new_cap * item_sz);
        h->cap = new_cap;
    }
    return h + 1;
}

void darr_sized_erase(void *darr, uint32 index, uint32 item_sz)
{
    if (!darr)
        return;
    darr_head_t *h = _darr_head(darr);
    if (index >= h->num)
        return;
    size_t  num_bytes   = item_sz * h->num - (index + 1);
    char    *mem        = (char*)darr;
    char    *ind_pos    = mem + index * item_sz;
    h->num--;
    memcpy(ind_pos, ind_pos + item_sz, num_bytes);
}

#define CFG_PARSE_BUF_SZ 1024

int parse_cfg_file(const char *path,
    void (*callback)(void *ctx, const char *opt, const char *val),
    void *context)
{
    if (!path)      return 1;
    if (!callback)  return 2;

    file_t *f = file_open(path, "r");
    if (!f)
        return 3;

    char    stack_buf[CFG_PARSE_BUF_SZ];
    int     ret         = 0;
    char    *buf        = stack_buf;
    char    *val        = 0;
    uint    buf_max     = CFG_PARSE_BUF_SZ;
    int     i           = 0;
    bool32  have_first  = 0;
    bool32  eof;

    for (;;)
    {
        int v   = file_getc(f);
        eof     = v == EOF || !v;
        buf[i]  = v;

        if (buf[i] == '\n' || eof)
        {
            if (val)
            {
                buf[i]      = 0;
                char *opt   = buf;
                str_strip_trailing_spaces(opt);
                str_strip_trailing_spaces(val);
                if (opt[0] && val[0] && !streq(val, "(null)"))
                    callback(context, opt, val);
            }
            i           = 0,
            val         = 0;
            have_first  = 0;

            if (eof)
                break;
            else
                continue;
        }

        if (!val)
        {
            if (!have_first && !(buf[i] == ' ' || buf[i] == '\t'))
            {
                if (buf[i] == '#') /* Line is a comment */
                {
                    int c = file_getc(f);;
                    for (; c != EOF && c != '\n' && c; c = file_getc(f));
                    i = 0;
                    continue;
                }
                have_first = 1;
            } else
            if (buf[i] == '=')
            {
                val     = buf + i + 1;
                buf[i]  = 0;
            }
        }

        if ((i++) != buf_max)
            continue;

        /* Buffer full - count the size required and attempt to heap alloc */
        long int    offset      = file_tell(f);
        int         line_len    = i;
        int         c;

        while ((c = file_getc(f)) != EOF && c && c != '\n') line_len++;
        file_seek(f, offset, FILE_SEEK_SET);

        int     buf_sz      = line_len * 150 / 100 + 1;
        size_t  val_offset  = (size_t)(val - buf);

        if (buf == stack_buf)
        {
            char *new_buf = malloc(buf_sz);
            if (!new_buf) {ret = 4; goto out;}
            memcpy(new_buf, buf, i);
            buf = new_buf;
        } else
        {
            buf = realloc(buf, buf_sz);
            if (!buf) {ret = 5; goto out;}
        }

        if (val) val = buf + val_offset;
        buf_max = buf_sz;
    }

    out:
        file_close(f);
        if (buf != stack_buf)
            free(buf);
        return ret;
}

int parse_def_file(const char *fp,
    int (*on_def)(void *ctx, const char *def, const char *val),
    int (*on_opt)(void *ctx, const char *opt, const char *val),
    void *ctx)
{
    #define FMT_ERR_IF(v_, ret_) \
    if ((v_)) \
    { \
        fmt_error = 1; \
        ret = ret_; \
        goto out; \
    } \

    int             ret         = 0;
    char            *line       = 0;
    bool32          fmt_error   = 0;
    int             c;

    file_t *f = file_open(fp, "r");
    if (!f)
    {
        printf("Failed to open def file %s.\n", fp);
        return 1;
    }

    darr_reserve(line, 256);

    for (;;)
    {
        darr_clear(line);

        /* Find the next non-whitespace */
        while ((c = file_getc(f)) != EOF && (c == ' ' || c == '\t' || c == '\n'));
        if (c == EOF)
            break;

        file_seek(f, file_tell(f) - 1, FILE_SEEK_SET);

        /* Read the line into a dynamic buffer */
        while ((c = file_getc(f)) != EOF && c != '\n')
           darr_push(line, (char)c);
        darr_push(line, 0); /* Zero-terminate */

        str_strip_trailing_spaces(line);
        _darr_head(line)->num = strlen(line) + 1;

        if (line[0] == '#') /* Line is a comment */
            continue;

        /* Line begins a new definition or is a value? 0 = def, 1 = value */

        int line_type           = -1;
        int first_space_index   = -1;
        int line_len            = (int)darr_num(line);
        int marker_index;

        for (int i = 0; i < line_len; ++i)
        {
            if (line[i] == ':')
            {
                line_type       = 0;
                marker_index    = i;
                break;
            }
            if (line[i] == '=')
            {
                line_type       = 1;
                marker_index    = i;
                break;
            }
            if ((line[i] == ' ' || line[i] == '\t') && first_space_index < 0)
                first_space_index = i;
        }

        FMT_ERR_IF(line_type < 0, 5);

        /* The line creates a new definition */
        if (line_type == 0)
        {
            char *id = line + marker_index + 1;
            str_strip_trailing_spaces(id);
            _darr_head(line)->num = strlen(line) + 1;
            line[first_space_index >= 0 ? first_space_index : marker_index] = 0;
            FMT_ERR_IF(!str_is_ascii(id), 6);
            FMT_ERR_IF(on_def(ctx, line, id), 7);
            continue;
        }

        int term = first_space_index >= 0 ? first_space_index : marker_index;
        line[term] = 0;

        char *value = line + term + 1;
        int j = 0;
        first_space_index = -1;

        while (*value != '=' && *value != '\n' && *value)
        {
            if (*value == ' ' && first_space_index < 0)
                first_space_index = j;
            value++, j++;
        }
        if (*value != '=')
            return 2;
        str_strip_trailing_spaces(++value);
        _darr_head(line)->num = strlen(line);
        FMT_ERR_IF(on_opt(ctx, line, value), 8);
    }

    out:
        if (fmt_error)
            DEBUG_PRINTFF("Format error in def file %s. Line "
                "contents: '%s', error %d.\n", fp, line, ret);
        if (ret)
            DEBUG_PRINTFF("Errors parsing def file %s, code %d.\n", fp, ret);
        file_close(f);
        darr_free(line);
        return ret;

    #undef FMT_ERR_IF
}

void str_to_upper(char *str)
    {for (char *c = str; *c; ++c) *c = (char)toupper(*c);}

void str_to_lower(char *str)
    {for (char *c = str; *c; ++c) *c = (char)tolower(*c);}

int str_to_bool(const char *str, bool32 *ret_bool)
{
    if (!str)
        return 1;
    char ts[7];
    strncpy(ts, str, 6);
    str_to_lower(ts);
    int ret = 0;
    if (streq(ts, "false") || streq(ts, "0"))
        *ret_bool = 0;
    else if (streq(ts, "true") || streq(ts, "1"))
        *ret_bool = 1;
    else
        ret = 2;
    return ret;
}

void str_strip_trailing_spaces(char *str)
{
    if (!str) return;

    int len, i;
    len = strlen(str);

    for (i = 0; i < len; ++i)
        if (str[i] != ' ' && str[i] != '\t' && str[i] != '\n')
            break;

    len = len - i;
    memmove(str, str + i, len);
    str[len] = '\0';

    for (i = len - 1; i >= 0; --i)
    {
        if (str[i] != ' ' && str[i] != '\t' && str[i] != '\n')
            break;
        str[i] = '\0';
    }


}

bool32 str_is_ascii(const char *str)
{
    for (const char *c = str; *c; ++c)
        if (!isascii(*c)) return 0;
    return 1;
}

int str_strip_non_ascii(char *str) {STRIP_STR(str, isascii);}
int str_strip_ctrl_chars(char *str) {STRIP_STR(str, NOT_CTRL);}

static void
_obj_pool_alloc_more(obj_pool_t *op, uint32 num_items)
{
    uint32 item_size = op->item_size;
    obj_pool_alloc_t alloc;
    alloc.mem = emalloc(num_items * item_size);
    alloc.num = num_items;
    darr_push(op->allocs, alloc);
    uint8 *item = (uint8*)alloc.mem;
    for (uint32 i = 0; i < num_items; ++i)
    {
        darr_push(op->free, (void*)item);
        item += item_size;
    }
    op->cap += num_items;
}

void
obj_pool_init(obj_pool_t *op, uint32 num_items, uint32 item_size)
{
    assert(item_size);
    memset(op, 0, sizeof(obj_pool_t));
    darr_reserve(op->allocs, 16);
    op->item_size   = item_size;
    num_items       = NEXT_POW2(num_items);
    if (num_items < 16)
        num_items = 16;
    _obj_pool_alloc_more(op, num_items);
}

void
obj_pool_destroy(obj_pool_t *op)
{
    for (uint32 i = 0; i < darr_num(op->allocs); ++i)
        free(op->allocs[i].mem);
    darr_free(op->allocs);
    darr_free(op->free);
    memset(op, 0, sizeof(obj_pool_t));
}

void
obj_pool_clear(obj_pool_t *op)
{
    darr_clear(op->free);
    uint32 item_sz = op->item_size;
    uint32 num_allocs = darr_num(op->allocs);
    for (uint32 i = 0; i < num_allocs; ++i)
    {
        uint32 num = op->allocs[i].num;
        for (uint32 j = 0; j < num; ++j)
            darr_push(op->free, (uint8*)op->allocs[i].mem + j * item_sz);
    }
}

void *
obj_pool_reserve(obj_pool_t *op)
{
    assert(op->item_size);
    void    *ret;
    uint32  num_free = darr_num(op->free);

    if (!num_free)
    {
        uint32 num_new = NEXT_POW2(op->cap) * 2;
        if (num_new < 16)
            num_new = 16;
        _obj_pool_alloc_more(op, num_new);
        num_free = _darr_head(op->free)->num;
    }

    ret = op->free[--num_free];
    _darr_head(op->free)->num = num_free;

    return ret;
}

void *
obj_pool_reserve_static(obj_pool_t *op)
{
    uint32 num_free = darr_num(op->free);
    if (!num_free)
        return 0;
    void *ret = op->free[--num_free];
    _darr_head(op->free)->num = num_free;
    return ret;
}

void
obj_pool_free(obj_pool_t *op, void *ptr)
    {darr_push(op->free, ptr);}

static dchar *_create_empty_dynamic_str_inner(uint len)
{
    uint32          cap = len + len % 4;
    dstr_header_t   *h  = malloc(sizeof(dstr_header_t) + cap + 1);
    if (!h)
        return 0;
    h->len = 0;
    h->cap = cap;
    return (dchar*)(h + 1);
}

static dstr_header_t *_grow_dynamic_str_cap(dstr_header_t *h, uint cap)
{
    uint32 max = cap * 150 / 100;
    uint32 req = cap + cap % 4;
    if (max < req)
        max = req;
    dstr_header_t *nh = realloc(h, sizeof(dstr_header_t) + max + 1);
    if (nh)
        nh->cap = max;
    return nh;
}
