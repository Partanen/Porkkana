PROGRAM_NAME 		= porkkana.exe

SDL2_INCLUDE_PATH 	= SDL2
SDL2_LIB_PATH		= SDL2\lib\x64
GLEW_INCLUDE_PATH   = libglew

COMPILER_FLAGS 		= /W3 /DEBUG /EHsc /Zi /Od /I $(SDL2_INCLUDE_PATH) \
                      /I $(GLEW_INCLUDE_PATH) /D_PORKKANA_DEBUG /DGLEW_NO_GLU \
                      /DGLEW_STATIC /D_CRT_SECURE_NO_WARNINGS /wd4244 /wd4267 \
                      /wd4312
LINKER_FLAGS		= /SUBSYSTEM:CONSOLE /DEBUG /LIBPATH:$(SDL2_LIB_PATH)
RUNDIR              = rundir
LIBS				= SDL2.lib SDL2main.lib opengl32.lib

COMPILE_OBJ			= cl /c $(COMPILER_FLAGS) src\$@.c /Fobuild\$@.obj

OBJS                = build\main.obj build\core.obj  build\bitmap.obj \
                      build\render.obj build\util.obj build\gui.obj \
                      build\assets.obj build\mainmenu.obj \
                      build\game.obj build\objs.obj build\glew.obj \
                      build\options.obj

all: $(OBJS) link

link:
	link $(LINKER_FLAGS) $(OBJS) $(LIBS) /out:$(RUNDIR)\$(PROGRAM_NAME)

build\main.obj: main
main: build
	$(COMPILE_OBJ)

build\core.obj: core
core: build
	$(COMPILE_OBJ)

build\render.obj: render
render: build
	$(COMPILE_OBJ)

build\bitmap.obj: bitmap
bitmap: build
	$(COMPILE_OBJ)

build\util.obj: util
util: build
	$(COMPILE_OBJ)

build\gui.obj: gui
gui: build
	$(COMPILE_OBJ)

build\assets.obj: assets
assets: build
	$(COMPILE_OBJ)

build\mainmenu.obj: mainmenu
mainmenu: build
	$(COMPILE_OBJ)

build\game.obj: game
game: build
	$(COMPILE_OBJ)

build\objs.obj: objs
objs: build
	$(COMPILE_OBJ)

build\glew.obj: glew
glew: build
	$(COMPILE_OBJ)

build\options.obj: options
options: build
	$(COMPILE_OBJ)

build:
	mkdir build

run: rundir\SDL2.dll
	start /B /D rundir rundir\$(PROGRAM_NAME)
	exit

rundir\SDL2.dll:
	copy SDL2\lib\x64\SDL2.dll rundir\SDL2.dll
