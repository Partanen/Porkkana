#ifndef OPTIONS_H
#define OPTIONS_H

typedef struct screen_t screen_t;

extern screen_t *options;

void options_open();
void options_update(double dt);

#endif /* OPTIONS_H */

