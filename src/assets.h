#ifndef ASSETS_H
#define ASSETS_H

typedef struct sfont_t          sfont_t;
typedef struct tex_t            tex_t;
typedef struct anim_t           anim_t;
typedef struct spritesheet_t    spritesheet_t;
typedef struct sprite_data_t    sprite_data_t;

struct spritesheet_t {
    tex_t               *ta;
    float               (*clips)[4];
    char                **clip_names;
    int                 num_clips;
};

struct sprite_data_t {
    tex_t   *tex;
    float   clip[4];
};

int as_init();
sfont_t *as_get_font(const char *name);
tex_t *as_get_tex(const char *name);
anim_t *as_get_anim(const char *name);
spritesheet_t *as_get_spritesheet(const char *name);
sprite_data_t spritesheet_get_data(spritesheet_t *ss, const char *name);

#endif /* ASSETS_H */
