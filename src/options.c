#include "options.h"
#include "core.h"
#include "gui.h"
#include "render.h"
#include "mainmenu.h"

const char *_str_true   = "true";
const char *_str_false  = "false";
#define BOOL_STR(v_) ((v_) ? _str_true : _str_false)

static screen_t _options = {
    "options",
    options_update,
    options_open,
    0
} ;

screen_t *options = &_options;

void options_open()
{
}

void options_update(double dt)
{
    gui_begin();

    gui_button_style(button_style1);
    gui_font(default_font);
    gui_origin(GUI_CENTER_CENTER);

    gui_textf("Maximize window: %s\n"
        "Window size: %dx%d", 0, 0, 0, BOOL_STR(config.maximize_window),
        config.window_size[0], config.window_size[1]);

    gui_origin(GUI_BOTTOM_RIGHT);
    if (gui_button("Back",  4, 4, 32, 14, 0))
        core_set_screen(mainmenu);

    r_viewport(0, 0, main_window_w, main_window_h);
    r_scissor(0, 0, main_window_w, main_window_h);
    r_color(0.f, 0.f, 0.f, 1.f);
    r_clear(R_CLEAR_COLOR_BIT);

    int vp[4];
    core_compute_target_viewport(vp);
    r_viewport(vp[0], vp[1], vp[2], vp[3]);
    r_scissor(vp[0], vp[1], vp[2], vp[3]);
    r_color(0.f, 0.f, 1.f, 1.f);
    r_clear(R_CLEAR_COLOR_BIT);

    gui_end();
}
