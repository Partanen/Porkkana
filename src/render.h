#ifndef RENDER_H
#define RENDER_H

#include "types.h"
#include "core.h"

#ifndef __ANDROID__
#define SB_USE_MAPPED_VBOS 0
#else
#define SB_USE_MAPPED_VBOS 0
#endif
/* Compile time switch for glMapBuffer() vs glBufferSubData() */

#define SB_USE_SAFE_LIMITS 1
/* Compile time switch for whether or not the sb_sprite functions will check if
 * there's enough space for a new sprite. If this is not turned on, it is up
 * to the caller to take care of possible overflows. */

#define SB_NUM_FLOATS_PER_QUAD 24
#define SIZE_OF_SPRITE (SB_NUM_FLOATS_PER_QUAD * sizeof(float))
#define NUM_TILETYPES 255
#define TEX_W(_tex_ptr) ((int)(tex_ptr)->w)
#define TEX_H(_tex_ptr) ((int)(tex_ptr)->h)
#define CLIP_W(clip) (clip[2] - clip[0])
#define CLIP_H(clip) (clip[3] - clip[1])
#define SFONT_CLIP(sf_ptr, index) \
    ((sf_ptr)->glyphs[(index) % (sf_ptr)->num_glyphs].clip)
#define SFONT_ADV(sf_ptr, index) \
    ((sf_ptr)->glyphs[(index) % (sf_ptr)->num_glyphs].advance)
#define SFONT_GLYPH(sf_ptr, index) \
    (&(sf_ptr)->glyphs[(index) % (sf_ptr)->num_glyphs])

/* Forward declaration(s) */
typedef struct gui_draw_list_t      gui_draw_list_t;
typedef struct world_t              world_t;
typedef struct img_t                img_t;
typedef struct tex_t                tex_t;
typedef struct ttf_t                ttf_t;
typedef struct sfont_t              sfont_t;          /* Spritefont type */
typedef struct sb_cmd_t             sb_cmd_t;

enum predef_shader_attr_loc
{
    SHADER_ATTR_LOC_POS = 0,
    SHADER_ATTR_LOC_UV,
    SHADER_ATTR_LOC_COL,

    NUM_PREDEF_SHADER_ATTR_LOCS
};

enum sb_flip_t
{
    SB_FLIP_NONE = 0,
    SB_FLIP_H,
    SB_FLIP_V,
    SB_FLIP_BOTH
};

enum tex_filter_t
{
    TEX_FILTER_LINEAR = 0,
    TEX_FILTER_NEAREST,
    NUM_TEX_FILTERS
};

enum tex_wrapping_t
{
    TEX_WRAP_REPEAT,
    TEX_WRAP_MIRROR_REPEAT,
    TEX_WRAP_CLAMP,
    TEX_WRAP_CLAMP_TO_EDGE,
    TEX_WRAP_CLAMP_TO_BORDER
};

extern int      R_CLEAR_COLOR_BIT;
extern int      R_CLEAR_DEPTH_BIT;
extern tex_t    *tile_sheet_tex;
extern img_t    *tile_sheet_img;
extern int      tile_px_w, tile_px_h, tile_top_px_h;

int
init_graphics_api(void *sdl_window);

int
tex_from_mem(tex_t *tex, uint8 *pixels, int w, int h, int px_format);

int
tex_from_mem_ext(tex_t *tex, uint8 *pixels, int w, int h,
    int px_format,
    enum tex_filter_t min_filter,
    enum tex_filter_t mag_filter,
    enum tex_wrapping_t wrapping);

int
tex_from_img(tex_t *tex, img_t *img);

int
tex_from_img_ext(tex_t *tex, img_t *img, int min_filter, int mag_filter, int wrapping);

void
tex_free(tex_t *tex);

#define tex_is_loaded(tex_) ((tex_)->id != 0)

int
sfont_from_ttf(sfont_t *sfont, ttf_t *ttf);

int
sfont_from_ttf_ext(sfont_t *sfont, ttf_t *ttf,
    enum tex_filter_t min_filter,
    enum tex_filter_t mag_filter,
    enum tex_wrapping_t wrapping);

void
sfont_free(sfont_t *sfont);

#define sfont_is_loaded(sf_) ((sf_)->glyphs ? 1 : 0)

int
sb_init(int max_sprites, char *err_log, int err_log_len);

void
sb_destroy();

int
sb_begin(uint32 shader, float *coord_space);
/* If shader is 0, the default shader will be used
 * If coord_space is 0, uses [x = 0, y = 0] with gl viewport width and height */

/* sprite draw function postfixes:
 * s: scale, parameters sx and sy
 * c: color, paremeter col (pointer to an array of 4 unsigned bytes)
 * r: rotate, paremeter angle in radians, ox and oy origin relative to the
 *    sprite's top left corner. ox and oy are NOT automatically multiplied by
 *    scale
 * f: flip, parameter flip - possibl e options are SB_FLIP_H and SB_FLIP_V
 *
 * Note: none of the arguments can be NULL. If SB_USE_SAFE_LIMITS is not
 * non-zero, the functions also do no checking on whether or not the batch still
 * has space for a new sprite, so overflow must be checked for by the caller. */

void
sb_sprite(tex_t *tex, float *clip, float x, float y, float z);

void
sb_sprite_s(tex_t *tex, float *clip, float x, float y, float z,
    float sx, float sy);

void
sb_sprite_c(tex_t *tex, float *clip, float x, float y, float z, uint8 *col);

void
sb_sprite_r(tex_t *tex, float *clip, float x, float y, float z,
    float angle, float ox, float oy);

void
sb_sprite_f(tex_t *tex, float *clip, float x, float y, float z,
    enum sb_flip_t flip);

void
sb_sprite_sc(tex_t *tex, float *clip, float x, float y, float z,
    float sx, float sy, uint8 *col);

void
sb_sprite_sr(tex_t *tex, float *clip, float x, float y, float z,
    float sx, float sy, float angle, float ox, float oy);

void
sb_sprite_sf(tex_t *tex, float *clip, float x, float y, float z,
    float sx, float sy, enum sb_flip_t flip);

void
sb_sprite_cr(tex_t *tex, float *clip, float x, float y, float z,
    uint8 *col, float angle, float ox, float oy);

void
sb_sprite_cf(tex_t *tex, float *clip, float x, float y, float z,
    uint8 *col, enum sb_flip_t flip);

void
sb_sprite_scr(tex_t *tex, float *clip, float x, float y, float z,
    float sx, float sy, uint8 *col, float angle, float ox, float oy);

void
sb_sprite_scf(tex_t *tex, float *clip, float x, float y, float z,
    float sx, float sy, uint8 *col, enum sb_flip_t flip);

void
sb_sprite_scrf(tex_t *tex, float *clip, float x, float y, float z,
    float sx, float sy, uint8 *col, float angle, float origin_x, float origin_y,
    enum sb_flip_t flip);

void
sb_text(sfont_t *sf, const char *txt, float x, float y, float z);

void
sb_text_sc(sfont_t *sf, const char *txt, float x, float y, float z,
    float sx, float sy, uint8 *col);

void
sb_text_wrap(sfont_t *sf, const char *txt, float x, float y, float z, float wrap);

void
sb_text_wrap_s(sfont_t *sf, const char *txt, float x, float y, float z,
    float wrap, float sx, float sy);

int
sb_end();

int
r_init(void *sdl_window);

int
r_destroy();

void
r_render_gui(gui_draw_list_t *lists, int num);

void
r_set_vsync(bool32 opt);

bool32
r_get_vsync();

void
r_color(float r, float g, float b, float a);

void
r_clear();

void
r_scissor(int x, int y, int w, int h);
/* From top left, unlike the default scissor */

void
r_get_scissor(int *x, int *y, int *w, int *h);

void
r_viewport(int x, int y, int w, int h);

void
r_get_viewport(int *x, int *y, int *w, int *h);

void
r_swap_buffers(void *sdl_window);

void
print_gl_errors();

uint32
shader_from_strs(const char *vert, const char *frag,
    char *err_log, int err_log_len);

uint32
shader_from_files(const char *vert_path, const char *frag_path,
    char *err_log, int err_log_len);

#endif /* RENDER_H */
