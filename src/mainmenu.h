#ifndef MAINMENU_H
#define MAINMENU_H

typedef struct screen_t screen_t;

extern screen_t *mainmenu;

void mainmenu_open();
void mainmenu_update(double dt);

#endif /* MAINMENU_H */
