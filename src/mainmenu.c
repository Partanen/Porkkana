#include <string.h>
#ifndef __ANDROID__
#include <SDL2/SDL.h>
#else
#include <SDL.h>
#endif
#include "core.h"
#include "mainmenu.h"
#include "gui.h"
#include "render.h"
#include "bitmap.h"
#include "assets.h"
#include "util.h"
#include "game.h"
#include "options.h"

screen_t _mainmenu = {
    "mainmenu",
    mainmenu_update,
    mainmenu_open,
    0
};

screen_t                *mainmenu = &_mainmenu;
static sprite_data_t    _porkkana_sd;
static tex_t            *_load_tex;

void mainmenu_open()
{
    spritesheet_t *ss = as_get_spritesheet("objects");
    if (ss)
        _porkkana_sd = spritesheet_get_data(ss, "carrot3");
    _load_tex   = as_get_tex("loading");
}

void mainmenu_update(double dt)
{
    int w = 84;
    int h = 32;
    gui_begin();
    if (core_is_key_down(SDL_SCANCODE_ESCAPE))
        core_shutdown();
    gui_button_style(button_style1);
    gui_font(default_font);
    gui_origin(GUI_CENTER_CENTER);

    if (gui_button("Play", 0, 0, w, h, 0))
        core_set_screen(game);
    if (gui_button("Options", 0, h + 2, w, h, 0))
        core_set_screen(options);
    if (gui_button("Exit", 0, h * 2 + 4, w, h, 0))
        core_shutdown();

    gui_texture_s(_porkkana_sd.tex, _porkkana_sd.clip, 0, -h - 60, 2.f,
        2.f);
    gui_text_s("PORKKANA", 0, 0, -h - 25, 3.f);

    r_viewport(0, 0, main_window_w, main_window_h);
    r_scissor(0, 0, main_window_w, main_window_h);
    r_color(0.f, 0.f, 0.f, 1.f);
    r_clear(R_CLEAR_COLOR_BIT);

    int vp[4];
    core_compute_target_viewport(vp);
    r_viewport(vp[0], vp[1], vp[2], vp[3]);
    r_scissor(vp[0], vp[1], vp[2], vp[3]);
    r_color(0.f, 0.f, 1.f, 1.f);

    if (_load_tex) {
        float fvp[4] = {0, 0, RESOLUTION_W, RESOLUTION_H};
        sb_begin(0, fvp);
        float clip[4] = {0, 0, _load_tex->w, _load_tex->h};
        sb_sprite_s(_load_tex, clip, 0, 0, 0,
            (float)RESOLUTION_W / _load_tex->w,
            (float)RESOLUTION_H / _load_tex->h);
        sb_end(0);
    } else {
        r_color(0.f, 0.f, 1.f, 1.f);
        r_clear(R_CLEAR_COLOR_BIT);
    }
    gui_end();
}
